"""
This module contains model of a deep neural network (DNN)
"""

class DNN:
    def __init__(self, name="DNN"):
        self.name = name
        self.__layers = []
        self.__connections = []

        self.__next_layer_id = 0

    def get_layer_input_connections(self, layer):
        layer_input_connections = []
        for connection in self.__connections:
            if connection.dst == layer:
                layer_input_connections.append(connection)
        return layer_input_connections

    def get_layer_output_connections(self, layer):
        layer_input_connections = []
        for connection in self.__connections:
            if connection.src == layer:
                layer_input_connections.append(connection)
        return layer_input_connections

    def get_layer_id(self, layer):
        l_id = 0
        for l in self.__layers:
            if l == layer:
                return l_id
            l_id = l_id + 1
        return -1

    def add_layer(self, layer):
        layer.id = self.__next_layer_id
        self.__layers.append(layer)
        self.__next_layer_id = self.__next_layer_id + 1

    def add_connection(self, connection):
        self.__connections.append(connection)

    def connect_layers(self, src_id, dst_id):
        src_layer = self.__layers[src_id]
        dst_layer = self.__layers[dst_id]
        connection = Connection(src_layer, dst_layer)
        self.__connections.append(connection)

    def stack_layer(self, layer):
        """
        Add layer and chain-connect it to the current dnn output layer
        :param layer: new layer
        """
        self.add_layer(layer)
        if len(self.__layers) > 1:
            src = self.__layers[-2]
            dst = self.__layers[-1]
            connection = Connection(src, dst)
            self.add_connection(connection)

    def get_layers(self):
        return self.__layers

    def get_connections(self):
        return self.__connections

    #give short description of the dnn
    def __str__(self):
        return "{name: " + self.name + ", layers: " + str(len(self.__layers)) + ", connections: " + str(len(self.__connections)) + "}"

    #print full description of the dnn
    def print_details(self, print_layers=True, print_connections=True):
        print(self)
        if print_layers:
            print("Layers: ")
            for layer in self.__layers:
                print("   ", layer)
        if print_connections:
            print("Connections: ")
            for connection in self.__connections:
                print("   ", connection)


class Layer:
    """
    A CNN layer
    """
    def __init__(self, res, op="conv", fs=3, ifm=1, ofm=3, bordermode="same"):
        #operator
        self.op = op
        #kernel (filter) size
        self.fs = fs
        #stride
        self.stride = 1
        #input feature maps
        self.ifm = ifm
        #output feature maps
        self.ofm = ofm
        #input image resolution (input image size)
        self.ih = self.iw = self.res = res
        #output image size
        self.ow = res
        self.oh = res
        # unique layer id within DNN. Set by DNN,
        # when layer is added to the DNN topology.
        self.id = id
        # borders processing: only valid for convolutional /pooling layers
        self.__bordermode=bordermode
        self.set_autopads()
        #padding
        self.pads = [0, 0, 0, 0]
        self.time_eval = 0

    """
    Data load computation/extraction formulas
    """
    def get_dim_names(self, data_type):
        if data_type == "input_data":
            return self.__get_inp_dim_names()
        if data_type == "output_data":
            return self.__get_outp_dim_names()
        if data_type == "weights":
            return self.__get_weight_dim_names()
        return []

    def __get_weight_dim_names(self):
        if self.op == "conv":
            return ["ifm", "ofm", "kh", "kw"]
        if self.op in ["gemm", "fc", "matmul"]:
            # NOTE: tied to gemm ops tensor
            return ["oh", "ow"]
        return []

    def __get_outp_dim_names(self):
        # NOTE: tied to gemm ops tensor
        if self.op in ["gemm", "fc", "matmul"]:
            return ["ow"]
        return ["ofm", "oh", "ow"]

    def __get_inp_dim_names(self):
        # NOTE: tied to gemm ops tensor
        if self.op in ["gemm", "fc", "matmul"]:
            return ["oh"]
        if self.op == "pool":
            return ["oh", "ow", "ofm", "kh", "kw"]
        return ["oh", "ow", "ifm"]

    """
    Printout
    """

    def __str__(self):
        return "{id:" + str(self.id) + ", op: " + self.op + ", ifm: " + str(self.ifm) + \
               ", ofm: " + str(self.ofm) + ", res: " + str(self.res) + ", fs: " + str(self.fs) + "}"

    """
    Pads processing
    """
    def set_border_mode(self, bordermode: str):
        if bordermode not in ["same", "full", "valid"]:
            raise Exception("Border mode setting error, mode " + bordermode +
                             " is unsupported. Please choose from [same, full, valid]")
        self.__bordermode = bordermode
        self.set_autopad()

    def set_autopads(self):
        self.pads = self.__get_autopads()

    def __get_autopads(self):
        if self.__bordermode == "same" and self.op == "conv":
            hpad = wpad = ((self.res - 1) * self.stride - self.res + self.fs)/2
            return [wpad, wpad, hpad, hpad]

        #default = valid
        return [0, 0, 0, 0]

    def get_hpad(self):
        return self.pads[1]

    def get_wpad(self):
        return self.pads[0]


class Connection:
    """
    A Connection between two DNN layers
    """
    def __init__(self, src: Layer, dst: Layer):
        self.src = src
        self.dst = dst

    def __str__(self):
        return "{src: " + str(self.src.id) + ", dst: " + str(self.dst.id) + "}"