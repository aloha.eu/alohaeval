ALOHA: unified architecture-aware evaluation model for CNNs execution on heterogeneous platforms
================================================================================================================================

The ALOHA evaluation tool, presented in this project enables for simple and efficient evaluation of platform-aware metrics 
of Convolutional Neural Networks (CNNs), executed on heterogeneous Edge Platforms. In particular, provided a CNN description 
(e.g. in ONNX format), the tool is able to evaluate one or several platform-aware metrics of a CNN:

- **latency** (milliseconds) characterizes time, required by a CNN to process one input data sample
- **throughput** (frames per second) characterized number of inputs, processed by a CNN per second
- **energy cost** (Joules) characterizes energy, consumed by a CNN to process one input data sample
- **memory cost** (MegaBytes) characterizes total amount of memory, required to deploy and execute CNN on the target platform

I. Requirements and installation
---------------------------------------------------------------------------------------------------------------------------------
### Requirements

Tool requires python 3.x and a list of python libraries, specified in the requirements.txt file, 
located in the root folder of the project.

For the latest 3.x python release, installation instructions, etc. visit https://www.python.org

For installation of python libraries, follow instructions in the **requirements.txt** file, 
located in the root folder of the project.

### Installation
ALOHA evaluation is a Python tool, it does not require any special installation and can be used 
right after you have python 3.x + libraries from requirements.txt installed.

II. User Manual
---------------------------------------------------------------------------------------------------------------------------------
### Interface

The tool has a command-line interface with API script, called **eval_aloha.py**, located in the root folder of the project.
To call the tool help, execute from terminal:

$ **eval_aloha.py** --help

### inputs and outputs

The tool accepts following inputs:
- a **CNN**: a CNN in [ONNX format](https://onnx.ai/) or a path to directory with several CNNs in ONNX format. 
  Test examples of CNNs in ONNX format are given in *input_examples/dnns/onnx_dnns*
- a **platform**: an ALOHA target platform specification. Project provides two example 
  platform specifications in  *input_examples/platforms* folder. The **jetson.json** specification
  describes NVIDIA [Jetson TX2](https://developer.nvidia.com/embedded/jetson-tx2) embedded platform. The 
  **neuraghe.json** specification describes [NEURAGHE FPGA-based platform](https://arxiv.org/abs/1712.00994)
- **l, t, m, e** (latency, throughput, memory, energy) flags, where each flag specifies corresponding platform-aware metric of a CNN to be evaluated.
- an [**execution configuration**]: an optional input, which specifies following details of CNN execution: 
    - *distribution of CNN operators over the target platform processors*;
    - *exploitation of task-level (pipeline) parallelism, available among layers of a CNN*. 
      
  The *distribution of CNN operators over the target platform processors* specifies how the CNN layers, 
  performing variousCNN operators, are distributed over heterogeneous processors of  
  a  target  platform.  This  parameter  is  important  for  precise assessment  of  CNN  metrics,  when  a  CNN  are  executed  on several  
  heterogeneous  processors  of  a  target  platform (e.g.,when  CNN  is  executed  on  an  FPGA-based  platform,  where only some CNN
  operators can be executed on the FPGA, and the rest of the CNN operators are executed on the platform CPUs). If If for a CNN operator *op*,
  no processor types are specified in the execution config, the tool assumes, that every layer, performing operator *op*, can be executed on every processor,
  available on target platform.
  
  The *exploitation of task-level (pipeline) parallelism* specifies if  a  CNN  is  executed  sequentially (layer-by-layer)  or  as  a  pipeline.  
  When  a CNN  is  executed  sequentially,  only  one  of  the  CNN  layers is  executed  at  every  moment  in  time.  This  type  of  CNN execution
  is  typical  for  the  majority  of  widely  used  Deep Learning frameworks.  The execution  of  a  CNN  as  a  pipeline  is  an  
  alternative  to  the sequential  CNN  execution.  When  a  CNN  is  executed  as  a pipeline,  several  CNN  layers  can  be  executed  in  parallel, 
  processing different inputs of a CNN. Execution of a CNN asa pipeline enables for higher throughput of CNNs, executed on heterogeneous target platform.

  An example of CNN execution configuration is given in *input_examples/exec_configs/config1.json*



As output, tool provides one or several lines of format:

{'id:': XX, 'latency (ms)': XX.XX, 'energy (J)': XX.XX, 'memory (MB): ':XX.XX}

- **id** : unique id of an input CNN. id = 0, if only one CNN was evaluated.
- **latency** (ms): evaluated CNN latency/execution time in milliseconds.
- **throughput** (fps): evaluated CNN throughput in frames per second.
- **energy** (J): evaluated CNN energy cost in Joules.
- **energy** (MB): evaluated CNN memory cost in Megabytes.

III. Examples
---------------------------------------------------------------------------------------------------------------------------------

### example 1: 

Estimate latency and memory of a CNN, specified in input_examples/dnns/onnx_dnns/o34461.onnx file and executed on Neuraghe platform, 
specified in input_examples/platforms/neuraghe.json 

*$ python eval_aloha.py --cnn input_examples/dnns/onnx_dnns/o34461.onnx -p input_examples/platforms/neuraghe.json -l -m*

Expected output:

{'id:': 0, 'latency (ms)': 4.7459, 'memory (MB)': 7.5116}

### example 2: 

Estimate, latency, throughput, energy cost  and memory cost of all CNNs, located in input_examples/dnns/onnx_dnns
and executed on Jetson TX 2 platform, specified in input_examples/platforms/neuraghe.json file:


*$ python eval_aloha.py --cnn input_examples/dnns/onnx_dnns -p input_examples/platforms/jetson.json -l -t -m -e*

Expected output: 

- {'id:': 0, 'latency (ms)': 2.5125, 'throughput(fps)': 398, 'energy (J)': 0.0377, 'memory (MB)': 10.3993}
- {'id:': 1, 'latency (ms)': 1.3328, 'throughput(fps)': 750, 'energy (J)': 0.0200, 'memory (MB)': 7.2073}
- {'id:': 2, 'latency (ms)': 4.4713, 'throughput(fps)': 223, 'energy (J)': 0.0671, 'memory (MB)': 18.2481}
- {'id:': 3, 'latency (ms)': 1.2685, 'throughput(fps)': 788, 'energy (J)': 0.0190, 'memory (MB)': 18.8425}
- {'id:': 4, 'latency (ms)': 1.1541, 'throughput(fps)': 866, 'energy (J)': 0.0174, 'memory (MB)': 15.0233}
- {'id:': 5, 'latency (ms)': 1.7163, 'throughput(fps)': 582, 'energy (J)': 0.0257, 'memory (MB)': 15.3877}
- {'id:': 6, 'latency (ms)': 3.8902, 'throughput(fps)': 257, 'energy (J)': 0.0584, 'memory (MB)': 26.7539}
- {'id:': 7, 'latency (ms)': 1.6517, 'throughput(fps)': 605, 'energy (J)': 0.0248, 'memory (MB)': 30.8684}

