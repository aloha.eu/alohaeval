"""
This module parses a .json description of DNN execution configuration and creates corresponding object of
ExecConfig class, defined in scheduling.execution_config.py
"""
from parsers.json_util import extract_or_default
from scheduling.execution_config import ExecConfig


def parse_json_exec_config(json_exec_config):
    exec_config = ExecConfig()
    exec_config.pipeline = extract_or_default(json_exec_config, "pipeline", False)
    exec_config.ops_dist = extract_or_default(json_exec_config, "ops_dist", {})
    return exec_config

