import json
"""
A bunch of common functions, useful for reading and parsing .json files
"""


def read_json(abs_path):
    """
    Read json file
    :param abs_path: abs path to .json file
    :return: json string, obtained from the file
    """
    with open(abs_path) as json_file:
        str_json = json.load(json_file)
    return str_json


def extract_or_default(json_str, attr, default_value):
    """
    Extract value from json string or return default value
    :param json_str: json string
    :param attr: attribute name
    :param default_value: default value
    :return: extracted attribute value or default value
    """
    if attr in json_str:
        return json_str[attr]
    return default_value


def parse_list(filepath):
    """
    Parse list, specified in a .json file
    :param filepath path to the .json file
    :return list, specified in the .json file
    """
    with open(filepath) as f:
        json_objs = json.load(f)
        return json_objs


