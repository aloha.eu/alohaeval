def print_distinct_param(distinct_layers, allowed_ops):
    """
    Print distinct parameters list for layers, grouped by ops
    :param distinct_layers: distinct CNN layers
    :param allowed_ops: CNN ops
    """
    for op in allowed_ops:
        print_distinct_param_one_op(distinct_layers, op)


def print_distinct_param_one_op(distinct_layers, op):
    """
    Print distinct parameters list for one operator
    :param distinct_layers: distinct CNN layers
    :param op: CNN op
    """
    fs = []
    ifms = []
    ofms = []
    ihs = []
    for layer in distinct_layers:
        if layer.op == op:
            # ifm: 64.0, ofm: 64.0, res: 32, fs: 2
            # print(layer)
            if layer.fs not in fs:
                fs.append(layer.fs)
            if layer.ifm not in ifms:
                ifms.append(layer.ifm)
            if layer.ofm not in ofms:
                ofms.append(layer.ofm)
            if layer.res not in ihs:
                ihs.append(layer.res)
    fs.sort()
    ifms.sort()
    ofms.sort()
    ihs.sort()
    print("distinct param: op ", op, "; fs: ", fs, "; ifm: ", ifms, "; ofm: ", ofms, "iw=ih: ", ihs)


def get_distinct_layers(dnns_list, allowed_ops=None):
    """
    Get list of distinct layers in dnns list
    :param dnns_list: dnns list
    :param allowed_ops: (optional) list of allowed layer operators: if None,
    layers with any operator are selected, otherwise only layers with allowed operators
    are selected
    :return: list of distinct dnn layers
    """
    def op_is_allowed(op, allowed_ops=None):
        if allowed_ops is None:
            return True
        return op in allowed_ops

    def layers_match(layer1, layer2):
        return layer1.op == layer2.op and layer1.ifm == layer2.ifm\
               and layer1.ofm == layer2.ofm and layer1.res == layer2.res \
               and layer1.fs == layer2.fs

    def has_layer(layers_list, new_layer):
        for list_layer in layers_list:
            if layers_match(list_layer, new_layer):
                return True
        return False

    distinct_layers = []

    for dnn in dnns_list:
        for layer in dnn.get_layers():
            if op_is_allowed(layer.op, allowed_ops):
                if not has_layer(distinct_layers, layer):
                    distinct_layers.append(layer)
    return distinct_layers






