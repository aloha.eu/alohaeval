from dnn.dnn import DNN, Layer
from parsers.json_util import parse_list


def parse_jetson_bm_as_annotated_dnn(filepath):
    dnn = DNN()
    layer_bms_list = parse_list(filepath)
    for layer_bm in layer_bms_list:
        layer = parse_jetson_layer_bm_record(layer_bm)
        dnn.add_layer(layer)
    return dnn


def parse_jetson_bm_as_layers_list(filepath):
    layers = []
    layer_bms = parse_list(filepath)
    for layer_bm in layer_bms:
        layer = parse_jetson_layer_bm_record(layer_bm)
        layers.append(layer)
    return layers


def parse_jetson_layer_bm_record(layer_desc):
    """
    Parse layer
    :param layer_desc json description of a CNN layer
    :return DNN layer as a class
    """
    op = layer_desc["op"]
    ifm = layer_desc["ch"]
    fs = layer_desc["kh"]
    ofm = layer_desc["ofm"]
    res = layer_desc["iw"]

    layer = Layer(res=res, op=op, fs=fs, ifm=ifm, ofm=ofm)

    layer.time_eval = layer_desc["time"]
    layer.oh = layer.ow = layer_desc["oh"]
    layer.ih = layer.res
    layer.set_autopads()

    return layer