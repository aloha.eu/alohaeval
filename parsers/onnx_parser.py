import onnx
from onnx import helper, shape_inference
from onnx import TensorProto
from dnn.dnn import DNN, Layer, Connection
from util import round_div

"""
This module parses a CNN model in ONNX format and represents it as a DNN class, defined in dnn.dnn.py
"""


def read_onnx(path_to_onnx_model):
    onnx_model = onnx.load(path_to_onnx_model)
    return onnx_model


def onnx_to_dnn(onnx_model):
    """
    Convert onnx model into dnn model
    :param onnx_model: onnx model
    :return: dnn as DNN class (see dnn.dnn.py)
    """
    check_consistency(onnx_model)
    onnx_with_df = set_dataflow(onnx_model)
    data_layout = "NCHW" # select out NCHW, NHWC. It seems, only NCHW is currently supported

    dnn = DNN()
    add_layers(onnx_with_df, dnn)
    all_output_names = find_all_output_names(onnx_model)
    set_layers_dataflow(onnx_model, dnn, all_output_names, data_layout)
    add_connections(onnx_model, dnn)
    # print(all_output_names)
    return dnn


def check_consistency(model):
    """Check the model consistency"""
    onnx.checker.check_model(model)


def set_dataflow(model, check=True):
    """
    Set dataflow (input and output tensor shapes) for every layer in .onnx DNN model
    :param model: onnx model with not-set input_examples flow
    :param check: check ONNX model consistency after setting onnx
    :return: onnx model with set input_examples flow
    """
    # Apply shape inference (input_examples formats computation) on the model
    inferred_model = shape_inference.infer_shapes(model)
    # Check the model
    if check:
        onnx.checker.check_model(inferred_model)
    # print('After shape inference, the shape info of Y is:\n{}'.format(inferred_model.graph.value_info))
    return inferred_model


def add_layers(onnx_model, dnn_model, data_layout="NCHW"):
    """
    Parse layers of onnx model and add them to the dnn model
    :param onnx_model: onnx model
    :param dnn_model: dnn model
    :param data_layout: tensor dims order (NCHW or NHWC)
    """
    for node in onnx_model.graph.node:
        # print(node)
        op = onnx_to_dnn_op(node)
        layer = Layer(res=1, op=op)
        dnn_model.add_layer(layer)


def add_connections(onnx_model, dnn_model):
    """
    Add connections to a dnn model with layers
    :param onnx_model: onnx model
    :param dnn_model: corresponding dnn model
    """
    node_id = 0
    for node in onnx_model.graph.node:
        src_layer_id = node_id
        output_names = node.output if node.output is not None else []
        for output_name in output_names:
            # find input owner
            dst_layer_ids = find_dst_layer_ids(onnx_model, output_name)
            for dst_layer_id in dst_layer_ids:
                dnn_model.connect_layers(src_layer_id, dst_layer_id)
        node_id = node_id + 1


def find_dst_layer_ids(onnx_model, input_name):
    """
    Find ids of all layers, which accept specified input
    :param onnx_model: onnx model
    :param input_name: input name
    :return: ids of all layers, which accept specified input
    """
    layer_ids = []
    node_id = 0
    for node in onnx_model.graph.node:
        input_names = node.input if node.input is not None else []
        if input_name in input_names:
            layer_ids.append(node_id)
        node_id += 1
    return layer_ids


def find_all_output_names(onnx_model):
    """
    Find names of all tensors, referenced as layer outputs
    :param onnx_model: onnx model
    :return: names of all tensors, referenced as layer outputs
    """
    all_outputs = []
    for node in onnx_model.graph.node:
        output_names = node.output
        if output_names is not None:
            for output_name in output_names:
                all_outputs.append(output_name)
    return all_outputs


def set_layers_dataflow(onnx_model, dnn_model, all_output_names, data_layout="NCHW"):
    """
    Set dataflow of CNN layers
    :param onnx_model: onnx model
    :param dnn_model: dnn model
    :param all_output_names names of dnn layer outputs
    :param data_layout: tensor dims order (NCHW or NHWC)
    :return list of layer names (in layer traverse order)
    """
    node_id = 0
    layers = dnn_model.get_layers()
    for node in onnx_model.graph.node:
        layer = layers[node_id]

        input_names = node.input if node.input is not None else []
        output_names = node.output if node.output is not None else []

        # print("input: ", input_names, " len = ", len(input_names), "output: ", output_names, "len", len(output_names))

        # set output tensor (if exists)
        if len(output_names) > 0:
            output_tensor = find_onnx_data_tensor(onnx_model, output_names[0])
            set_output_tensor(layer, output_tensor, data_layout)

        # set input tensor (if exists)
        if len(output_names) > 0:
            # layer has single input
            if len(input_names) == 1:
                input_tensor = find_onnx_data_tensor(onnx_model, input_names[0])
                set_input_tensor(layer, input_tensor, data_layout)
            else:
                # layer has several inputs, among which are layer input and layer parameters (weights and biases)
                for input_name in input_names:
                    # input is an output of another tensor
                    if input_name in all_output_names:
                        input_tensor = find_onnx_data_tensor(onnx_model, input_name)
                        set_input_tensor(layer, input_tensor)

        # set layer kernel size
        layer_param_inputs = [input_name for input_name in input_names if input_name not in all_output_names]
        set_kernel_size(layer, layer_param_inputs, node)

        node_id = node_id + 1
        # input_tensor = find_onnx_data_tensor(onnx_model, input_names)
        # set_input_tensor(layer, input_tensor)


def set_input_tensor(layer, onnx_tensor, data_layout="NCHW"):
    """
    Extract and set layer input_examples dims from input tensor
    :param layer: DNN layer
    :param onnx_tensor: onnx tensor
    :param data_layout: tensor dims order (NCHW or NHWC)
    """
    shape = extract_onnx_tensor_shape(onnx_tensor)
    nchw_dims = tensor_shape_to_nchw(shape, onnx_tensor, data_layout)
    if nchw_dims is None:
        return
    i_n, i_c, i_h, i_w = nchw_dims
    layer.ifm = i_c
    layer.ih = i_h
    layer.iw = i_w
    layer.res = i_w
    # print("res: ", i_w)
    # print("set inp tensor ", nchw_dims, "to layer", layer.id)


def set_output_tensor(layer, onnx_tensor, data_layout="NCHW"):
    """
    Extract and set layer input_examples dims from output tensor
    :param layer: DNN layer
    :param onnx_tensor: onnx tensor
    :param data_layout: tensor dims order (NCHW or NHWC)
    """
    shape = extract_onnx_tensor_shape(onnx_tensor)
    nchw_dims = tensor_shape_to_nchw(shape, onnx_tensor, data_layout)
    if nchw_dims is None:
        return 
    o_n, o_c, o_h, o_w = nchw_dims
    layer.ofm = o_c
    layer.oh = o_h
    layer.ow = o_w
    # print("setting nchw dims: ", nchw_dims)


def set_kernel_size(layer, layer_param_inputs, onnx_node):
    """
    Set layer kernel size
    :param layer_param_inputs
    :param layer: layer
    :param onnx_node: corresponding onnx node
    """
    if layer.op not in ["conv", "pool"]:
        layer.fs = 1
        return
    if layer.op == "conv":
        # print(onnx_node)
        default_fs = 3
        fs = extract_kernel_size_from_attributes(onnx_node)
        layer.fs = fs if fs > 1 else default_fs
    if layer.op == "pool":
        default_fs = 2
        fs = extract_kernel_size_from_attributes(onnx_node)
        layer.fs = fs if fs > 1 else default_fs


def extract_kernel_size_from_attributes(onnx_node):
    """
    Extract pool/conv operator kernel size from onnx node attributes
    :param onnx_node: onnx node
    :return: kernel size or -1 (if kernel size is not found)
    """
    not_found = -1
    try:
        for attribute in onnx_node.attribute:
            if attribute.name == "kernel_shape":
                first_dim = attribute.ints[0]
                if first_dim is not None:
                    return first_dim
    except Exception:
        pass
    return not_found


def tensor_shape_to_nchw(tensor_shape, tensor, data_layout="NCHW"):
    """
    Represent tensor shape as a [n, c, h, w] dims
    :param tensor_shape: tensor shape,
    :param data_layout: tensor dims order (NCHW or NHWC)
    :return:
    """
    if tensor_shape is None:
        return None
    if len(tensor_shape) == 4:
        if data_layout == "NCHW":
            return tensor_shape
        else:
            # NHWC
            n = tensor_shape[0]
            c = tensor_shape[3]
            h = tensor_shape[1]
            w = tensor_shape[2]
            return [n, c, h, w]

    if len(tensor_shape) == 3:
        if data_layout == "NCHW":
            c = tensor_shape[0]
            h = tensor_shape[1]
            w = tensor_shape[2]
        else:
            # NHWC
            c = tensor_shape[2]
            h = tensor_shape[1]
            w = tensor_shape[0]
        return [c, c, h, w]

    if len(tensor_shape) == 2:
        n = tensor_shape[0]
        c = tensor_shape[1]
        h = 1
        w = 1
        return [n, c, h, w]

    if len(tensor_shape) == 1:
        n = tensor_shape[0]
        c = 1
        h = 1
        w = 1
        return [n, c, h, w]

    raise Exception("data tensor" + str(tensor) + " of shape " + str(tensor_shape) +
                    " has unexpected tensor len: " + str(len(tensor_shape)))


def extract_onnx_tensor_shape(onnx_tensor):
    """
    Extract shape (list of dimension sizes) of onnx tensor
    :param onnx_tensor: onnx tensor
    :return: list of onnx tensor dimension sizes
    """
    if onnx_tensor is None:
        return None
    try:
        shape = onnx_tensor.type.tensor_type.shape
        shape_dims = []
        for dim in shape.dim:
            dim_value = dim.dim_value
            if dim_value >0:
                shape_dims.append(dim_value)
        return shape_dims
    except Exception:
        return None


def onnx_to_dnn_op(onnx_node) -> str:
    """
    Extract operator from onnx node description
    :param onnx_node: onnx node
    :return: DNN operator
    """
    onnx_op = onnx_node.op_type if onnx_node.op_type else "none"
    op = onnx_op
    if onnx_op.lower() in ["conv"]:
        op = "conv"
    if onnx_op.lower() in ["gemm", "fc", "matmul"]:
        op = "gemm"
    if onnx_op.lower() in ["maxpool", "averagepool", "globalaveragepool"]:
        op = "pool"
    if onnx_op.lower() in ["batchnormalization", "bn"]:
        op = "bn"
    return op


def find_onnx_data_tensor(onnx_model, tensor_name):
    """
    Parse layers of onnx model and add them to the dnn model
    :param onnx_model: onnx model
    :param dnn_model: dnn model
    """
    value_info = onnx_model.graph.value_info
    for vi in value_info:
        if vi.name == tensor_name:
            return vi
    return None


def test():
    onnx_example_path = "/home/svetlana/ONNX/Dolly_onnx/onnx/o1.onnx"
    onnx_model = onnx.load(onnx_example_path)
    onnx_model = set_dataflow(onnx_model)
    dnn_model = onnx_to_dnn(onnx_model)
    dnn_model.print_details()

    # print('After shape inference, the shape info of Y is:\n{}'.format(onnx_model.graph.value_info))
    # for vi in onnx_model.graph.value_info:
    #    print(vi.name)

    # for node in onnx_model.graph.node:
    #    print(node)

    # print(onnx_model.graph)

# test()



