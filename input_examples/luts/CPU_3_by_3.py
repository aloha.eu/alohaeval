conv_3_by_3_CPU = [{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.207226}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.112109}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.167209}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.220672}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.26445}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.447449}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.204337}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.121339}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.129045}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.171878}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.22973}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.411479}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.171421}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.141301}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.146311}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.202321}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.290995}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.563232}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.159724}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.171596}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.185143}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.25974}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.44916}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.855109}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.192541}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.263097}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.330029}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.488356}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.860494}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.32636}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.295743}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.450536}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.674184}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.915835}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.35215}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 2.24208}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.867377}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.492999}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 1.05454}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 1.6932}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.3325}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 6.34837}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.797597}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.630853}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 0.933808}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 1.50672}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.21991}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 6.25416}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.947393}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 0.915614}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 1.28795}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 2.11458}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 4.12723}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 7.73463}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 1.29682}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 1.5613}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.27765}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 3.43274}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 5.94454}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 10.7778}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.94082}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 3.30931}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 4.21003}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 5.92763}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 9.65484}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 16.8849}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 3.44745}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 8.47181}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 7.91785}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 10.8309}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 17.1471}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 29.1238}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.38017}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 1.80345}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.98351}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 7.42613}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 13.677}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 26.4869}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.12674}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 2.4022}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.97003}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 7.22819}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 13.862}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 26.5961}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 3.83602}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 4.01581}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 5.91416}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 9.86792}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 17.124}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 34.6981}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 5.20405}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 6.91133}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 9.44699}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 14.529}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 24.3519}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 45.1825}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 7.84152}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 13.9055}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 17.655}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 25.0285}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 39.3025}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 68.3559}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 13.7832}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 37.472}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 33.2226}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 45.639}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 70.2457}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 118.771}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 7.29518}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.96836}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 8.68021}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 13.5702}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 26.7251}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 54.103}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 6.84755}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 5.53619}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 8.90544}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 14.8954}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 28.0132}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 53.8516}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 8.38688}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 8.57881}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 12.9167}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 19.2651}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 34.7405}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 67.3203}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 11.4739}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 14.638}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 20.5711}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 29.7053}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 50.6082}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 93.9771}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 17.6117}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 27.692}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 37.7438}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 52.2195}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 84.915}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 151.239}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 30.4104}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 80.5953}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 70.8908}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 98.9733}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 155.277}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 265.275}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 12.3167}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 7.52477}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 17.2548}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 28.1541}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 52.9901}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 109.927}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 11.5357}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 9.90466}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 17.8584}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 28.6366}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 52.1512}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 109.54}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 14.2625}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 16.9817}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 26.0786}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 40.8416}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 69.3097}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 135.428}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 19.6256}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 29.6004}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 41.3646}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 60.591}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 97.4491}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 184.021}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 30.0719}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 55.8918}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 72.7577}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 101.27}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 160.61}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 286.391}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 53.6493}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 160.189}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 142.526}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 193.18}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 293.032}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 501.807}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 36.3047}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 21.4358}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 46.0538}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 76.3361}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 147.117}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 292.221}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 34.7061}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 29.8804}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 48.5781}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 78.6048}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 149.73}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 291.561}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 42.962}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 47.267}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 69.291}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 107.174}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 190.967}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 386.636}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 58.4028}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 81.6098}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 113.053}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 165.17}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 280.109}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 511.79}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 89.9018}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 155.176}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 200.128}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 281.812}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 458.477}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 813.612}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 159.116}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 434.567}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 373.62}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 524.518}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 831.536}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1457.84},



{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 8, "hpad": 2, "wpad": 2, "time": 0.158599}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 8, "hpad": 2, "wpad": 2, "time": 0.200815}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 8, "hpad": 2, "wpad": 2, "time": 0.274732}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 8, "hpad": 2, "wpad": 2, "time": 0.381168}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 8, "hpad": 2, "wpad": 2, "time": 0.585303}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 8, "hpad": 2, "wpad": 2, "time": 1.31055}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 16, "hpad": 2, "wpad": 2, "time": 0.113125}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 16, "hpad": 2, "wpad": 2, "time": 0.178904}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 16, "hpad": 2, "wpad": 2, "time": 0.292468}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 16, "hpad": 2, "wpad": 2, "time": 0.437566}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 16, "hpad": 2, "wpad": 2, "time": 0.759637}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 16, "hpad": 2, "wpad": 2, "time": 1.54789}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 32, "hpad": 2, "wpad": 2, "time": 0.13423}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 32, "hpad": 2, "wpad": 2, "time": 0.215777}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 32, "hpad": 2, "wpad": 2, "time": 0.334597}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 32, "hpad": 2, "wpad": 2, "time": 0.53228}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 32, "hpad": 2, "wpad": 2, "time": 1.01974}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 32, "hpad": 2, "wpad": 2, "time": 2.08358}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 64, "hpad": 2, "wpad": 2, "time": 0.159876}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 64, "hpad": 2, "wpad": 2, "time": 0.294487}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 64, "hpad": 2, "wpad": 2, "time": 0.479853}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 64, "hpad": 2, "wpad": 2, "time": 0.855005}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 64, "hpad": 2, "wpad": 2, "time": 1.63061}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 64, "hpad": 2, "wpad": 2, "time": 3.27632}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 128, "hpad": 2, "wpad": 2, "time": 0.219494}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 128, "hpad": 2, "wpad": 2, "time": 0.433674}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 128, "hpad": 2, "wpad": 2, "time": 0.752821}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1.39843}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 128, "hpad": 2, "wpad": 2, "time": 2.76695}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 128, "hpad": 2, "wpad": 2, "time": 5.42523}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 8, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 256, "hpad": 2, "wpad": 2, "time": 0.341061}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 16, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 256, "hpad": 2, "wpad": 2, "time": 0.729764}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 32, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1.41213}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 256, "hpad": 2, "wpad": 2, "time": 2.61292}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 256, "hpad": 2, "wpad": 2, "time": 4.94353}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 5, "kw": 5,"oh":8, "ow": 8, "ofm": 256, "hpad": 2, "wpad": 2, "time": 9.6982}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 1.11665}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 2.11931}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 3.57442}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 6.07949}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 11.9754}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 23.7784}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 1.06694}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 2.60787}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 4.61336}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 8.16966}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 15.9235}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 31.5537}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 1.44508}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 3.37783}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 6.02673}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 10.9814}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 21.4441}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 42.7157}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 2.26511}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 5.01202}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 9.29481}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 17.4717}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 34.5267}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 68.6897}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 3.8833}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 8.21879}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 15.4612}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 29.4784}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 58.3057}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 116.118}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 6.99213}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 14.4215}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 28.076}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 54.5308}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 108.129}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 215.899}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 4.63144}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 8.98094}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 14.9459}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 26.4712}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 51.0211}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 103.063}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 4.70045}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 10.6519}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 18.9089}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 34.9473}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 68.1692}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 135.996}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 6.36344}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 13.8906}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 24.8868}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 46.676}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 91.8106}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 182.769}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 9.57319}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 20.695}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 38.7072}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 74.0944}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 145.867}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 292.143}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 16.1918}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 34.2}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 64.4476}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 123.916}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 246.563}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 492.647}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 29.2321}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 60.5456}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 118.406}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 230.603}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 459.423}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 919.552}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 10.3445}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 19.8676}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 33.9292}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 59.5766}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 118.284}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 235.073}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 10.7089}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 24.0572}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 42.7163}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 78.1468}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 156.875}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 311.28}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 14.1489}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 31.3678}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 56.6106}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 105.217}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 208.866}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 418.321}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 21.4741}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 46.9147}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 88.7868}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 167.061}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 333.698}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 668.993}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 36.7197}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 77.7305}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 147.423}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 282.976}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 564.735}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1128.27}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 67.0652}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 138.512}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 271.193}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 527.117}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1053.84}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 2109.53}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 18.1139}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 35.499}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 61.041}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 108.128}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 211.58}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 425.123}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 18.4232}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 42.9867}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 77.5087}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 142.326}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 279.015}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 559.384}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 25.0655}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 56.1372}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 102.105}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 190.024}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 375.949}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 754.477}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 38.3892}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 88.2544}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 159.943}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 302.516}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 598.593}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 1210.29}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 65.7245}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 139.33}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 265.707}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 511.327}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1018.68}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 2035.6}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 119.837}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 248.079}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 490.538}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 952.323}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1906.88}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 3813.01}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 58.0379}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 109.314}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 185.045}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 329.272}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 674.658}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 1338.01}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 55.9943}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 137.655}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 239.52}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 430.764}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 879.03}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 1756.49}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 76.1882}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 172.709}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 311.939}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 591.368}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 1195.64}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 2391.85}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 117.177}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 263.17}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 488.759}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 925.827}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 1875.69}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 3719.01}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 201.648}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 429.856}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 815.24}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1572.02}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 3140.97}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 6279.17}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 371.379}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 766.672}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1504.63}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 2928.03}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 5861.99}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 11726.8}]







