convs_Paola_CPU = [{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.0835656}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.0938085}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.0984833}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.113977}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.113692}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.142707}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.135676}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.173406}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.225039}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.0581725}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.109727}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.0900674}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.104755}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.104075}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.142873}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.165831}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.301035}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.386}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.0601826}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.0891481}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.0814944}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.0938666}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.113407}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.125794}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.16168}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.298292}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.399501}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.0575819}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.0859105}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.0906288}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.101067}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.108479}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.184094}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.287313}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.414625}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.556951}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.0575354}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.0888602}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.091967}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.109689}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.144522}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.265623}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.350917}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.497638}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.60505}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.0581986}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.105852}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.122227}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.158887}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.268466}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.360235}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.486674}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.633963}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.826572}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.0576721}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.137581}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.167416}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.256416}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.355985}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.529935}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.604415}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.816733}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.97521}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.0767439}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.194808}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.275843}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.426762}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.530121}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.663146}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.809356}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.08563}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.37446}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 3, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.100179}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 48, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.299252}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 64, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.425697}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 96, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.572858}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 128, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.661195}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 192, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.842598}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 256, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.995259}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 384, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.35344}
,
{"op": "conv", "ih": 2, "iw": 2, "ch": 512, "kh": 3, "kw": 3,"oh":2, "ow": 2, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.71014}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.0601128}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.111458}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.104933}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.126224}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.15688}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.201316}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.252151}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.356947}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.493665}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.0640168}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.127426}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.141043}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.16575}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.195797}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.265879}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.358571}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.528253}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.728606}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.0710364}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.130675}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.150218}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.187797}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.21683}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.295322}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.399912}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.617285}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.815837}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.0629579}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.152054}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.171096}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.224219}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.3205}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.407667}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.560108}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.79157}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.995027}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.0717519}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.173834}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.20403}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.252771}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.340974}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.481199}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.613853}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.874097}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.17221}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.068709}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.227483}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.269242}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.383763}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.480812}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.646425}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.816992}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.15954}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.51477}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.0773461}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.286377}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.365998}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.519648}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.595461}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.818467}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.02183}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.45798}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.89602}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.0983291}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.451902}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.505694}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.667696}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.78103}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.0788}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.34841}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.98605}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 384, "hpad": 1, "wpad": 1, "time": 2.53591}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 3, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.103103}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 48, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.623723}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 64, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.77102}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 96, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.94255}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 128, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.10818}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 192, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.46616}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 256, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.81108}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 384, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 2.62396}
,
{"op": "conv", "ih": 4, "iw": 4, "ch": 512, "kh": 3, "kw": 3,"oh":4, "ow": 4, "ofm": 512, "hpad": 1, "wpad": 1, "time": 3.30635}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.0849883}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.129229}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.159265}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.156123}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.235157}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.290583}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.401506}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.623566}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.932965}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.0951}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.162974}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.203896}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.252809}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.327519}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.507311}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.650923}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.951908}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 48, "hpad": 1, "wpad": 1, "time": 1.38009}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.106268}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.191716}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.238607}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.31357}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.403743}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.595394}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.797728}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 1.11937}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 64, "hpad": 1, "wpad": 1, "time": 1.60305}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.109456}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.255442}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.319409}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.415248}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.561367}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.793167}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.981657}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 1.4599}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 96, "hpad": 1, "wpad": 1, "time": 1.95956}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.12145}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.338943}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.43441}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.589693}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.743771}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.02592}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.29263}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.82824}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 128, "hpad": 1, "wpad": 1, "time": 2.43673}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.140909}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.458122}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.607316}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.810473}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.0471}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.35077}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.89293}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 2.58753}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 192, "hpad": 1, "wpad": 1, "time": 3.39282}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.1689}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.755189}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.965057}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.20367}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.49347}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 2.0062}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 2.49429}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 3.34034}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 256, "hpad": 1, "wpad": 1, "time": 4.42303}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.201839}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.13364}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.36362}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 1.71618}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 2.08595}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 2.84826}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 3.50217}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 4.74391}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 384, "hpad": 1, "wpad": 1, "time": 6.08931}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 3, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.26142}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 48, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.52687}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 64, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 1.80245}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 96, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 2.25586}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 128, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 2.75212}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 192, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 3.74643}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 256, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 4.57892}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 384, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 6.15799}
,
{"op": "conv", "ih": 8, "iw": 8, "ch": 512, "kh": 3, "kw": 3,"oh":8, "ow": 8, "ofm": 512, "hpad": 1, "wpad": 1, "time": 7.88397}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.202738}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.348058}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.386076}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.522627}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.681555}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.955623}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 1.44274}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 2.35275}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 3, "hpad": 1, "wpad": 1, "time": 3.31009}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.241365}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.519957}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.617646}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.861454}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 1.12788}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 1.69579}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 2.34164}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 3.31587}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 48, "hpad": 1, "wpad": 1, "time": 4.83705}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.256233}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.675106}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.833742}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 1.1343}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 1.47963}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.10629}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.83162}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 4.28996}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 6.0783}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.307281}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.943298}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 1.12537}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 1.58011}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 2.02268}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 2.79786}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 3.66651}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 5.02257}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 96, "hpad": 1, "wpad": 1, "time": 6.99043}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 0.350676}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.242}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.52646}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 2.07177}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 2.55354}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 3.49355}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 4.49411}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 6.5881}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 8.80053}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 0.430468}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.87988}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 2.29019}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 2.94347}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 3.60993}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 4.84873}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 6.11752}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 8.33086}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 192, "hpad": 1, "wpad": 1, "time": 11.1742}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 0.4921}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 2.60176}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 3.03285}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 3.87444}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 4.70174}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 6.29317}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 7.93509}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 11.0237}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 14.6555}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 0.608692}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 3.72829}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 4.20966}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 5.24661}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 6.31108}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 8.42476}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 10.6237}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 15.0037}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 384, "hpad": 1, "wpad": 1, "time": 19.7224}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 3, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 0.812422}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 48, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 5.11024}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 5.7757}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 96, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 7.0985}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 8.51032}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 192, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 11.2109}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 14.1349}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 384, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 19.969}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 512, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 512, "hpad": 1, "wpad": 1, "time": 25.9248}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 0.625137}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 1.1199}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 1.68121}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 2.07429}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 3.01181}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 4.136}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 6.1097}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 8.78469}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 3, "hpad": 1, "wpad": 1, "time": 13.3266}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 0.746698}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 2.36298}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 2.77007}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 3.77432}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 4.9454}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 6.60176}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 9.25713}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 13.3493}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 48, "hpad": 1, "wpad": 1, "time": 19.3296}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 0.85106}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.94362}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 3.49021}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 4.66817}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 5.95131}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 7.87877}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 10.7881}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 17.0503}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 23.9736}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 0.961337}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 4.03709}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 4.74051}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 6.18873}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 7.76055}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 10.4014}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 13.9362}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 19.9444}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 96, "hpad": 1, "wpad": 1, "time": 27.83}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.11699}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 5.23476}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 6.07632}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 7.83774}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 9.74874}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 13.05}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 17.1792}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 25.9475}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 34.9966}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 1.48091}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 7.49115}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 8.53437}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 11.1009}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 13.6072}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 17.9994}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 23.4039}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 33.0797}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 192, "hpad": 1, "wpad": 1, "time": 44.4724}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.79737}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 9.84896}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 11.3521}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 14.6898}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 17.7534}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 23.2754}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 29.933}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 43.7287}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 59.5241}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 2.41115}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 14.5944}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 16.4681}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 20.6034}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 24.9706}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 33.1603}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 42.2499}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 59.8342}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 384, "hpad": 1, "wpad": 1, "time": 78.3218}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 3, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 3.43943}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 48, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 20.6759}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 23.1567}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 96, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 28.4051}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 34.2833}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 192, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 44.7501}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 56.7211}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 384, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 79.9941}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 512, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 512, "hpad": 1, "wpad": 1, "time": 103.715}
]
