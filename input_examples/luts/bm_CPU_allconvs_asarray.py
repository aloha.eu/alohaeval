bm_trt_allConvs_CPU = [{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.88853}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 8, "hpad": 1, "wpad": 1, "time": 2.46467}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.28666}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.87599}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 8, "hpad": 1, "wpad": 1, "time": 5.26473}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 8, "hpad": 1, "wpad": 1, "time": 6.33717}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 16, "hpad": 1, "wpad": 1, "time": 1.89141}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 16, "hpad": 1, "wpad": 1, "time": 2.07688}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.07824}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.01016}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.94401}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 16, "hpad": 1, "wpad": 1, "time": 5.11425}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 32, "hpad": 1, "wpad": 1, "time": 2.11567}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 32, "hpad": 1, "wpad": 1, "time": 2.78793}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 32, "hpad": 1, "wpad": 1, "time": 2.70199}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 32, "hpad": 1, "wpad": 1, "time": 4.04553}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 32, "hpad": 1, "wpad": 1, "time": 5.03814}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 32, "hpad": 1, "wpad": 1, "time": 7.3303}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.06675}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.66879}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 4.10207}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 4.61949}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 6.47755}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 64, "hpad": 1, "wpad": 1, "time": 7.78243}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 1.42889}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 2.79977}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 4.63481}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 6.61652}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 9.04808}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 128, "hpad": 1, "wpad": 1, "time": 7.15759}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1.04943}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 4.7069}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 5.97825}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 8.50448}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 7.61674}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 3, "kw": 3,"oh":16, "ow": 16, "ofm": 256, "hpad": 1, "wpad": 1, "time": 12.4011}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.878177}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 0.901739}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 2.40586}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 4.65546}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 6.18112}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 8, "hpad": 1, "wpad": 1, "time": 9.51576}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 1.3087}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 1.60497}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 2.25523}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 4.12043}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 6.61713}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 16, "hpad": 1, "wpad": 1, "time": 9.96764}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 1.62776}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 2.36476}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 4.6656}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 6.23944}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 8.04289}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 32, "hpad": 1, "wpad": 1, "time": 12.3815}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 2.01136}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 4.08319}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 5.93486}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 8.2781}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 11.0855}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 64, "hpad": 1, "wpad": 1, "time": 14.9852}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 2.50488}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 6.04959}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 8.16866}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 10.183}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 12.7716}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 128, "hpad": 1, "wpad": 1, "time": 19.5185}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 3.72452}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 10.7887}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 11.884}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 17.1639}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 22.8575}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 3, "kw": 3,"oh":32, "ow": 32, "ofm": 256, "hpad": 1, "wpad": 1, "time": 33.5285}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 3.05285}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 2.18361}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 4.97179}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 8.91369}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 14.1994}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 8, "hpad": 1, "wpad": 1, "time": 27.3939}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.29059}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 3.88922}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 5.99045}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 10.4946}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 14.9474}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 16, "hpad": 1, "wpad": 1, "time": 28.7728}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 4.14162}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 5.29662}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 8.25786}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 11.7459}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 19.9707}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 32, "hpad": 1, "wpad": 1, "time": 34.8303}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 5.81219}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 7.90256}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 11.7882}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 17.8928}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 26.4492}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 64, "hpad": 1, "wpad": 1, "time": 50.4502}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 8.47361}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 15.8795}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 20.1579}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 27.1619}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 44.4349}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 128, "hpad": 1, "wpad": 1, "time": 74.2434}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 13.9709}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 37.6346}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 36.0891}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 51.3988}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 72.5294}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 3, "kw": 3,"oh":64, "ow": 64, "ofm": 256, "hpad": 1, "wpad": 1, "time": 122.881}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 6.81094}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 5.31366}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 10.3381}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 16.8735}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 28.6576}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 8, "hpad": 1, "wpad": 1, "time": 57.2101}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 8.01012}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 7.05694}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 11.4797}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 16.9069}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 30.0915}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 16, "hpad": 1, "wpad": 1, "time": 58.755}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 9.41935}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 10.1296}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 14.5697}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 22.5044}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 38.9643}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 32, "hpad": 1, "wpad": 1, "time": 71.6472}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 13.0895}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 17.8635}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 23.4637}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 34.4461}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 54.6721}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 64, "hpad": 1, "wpad": 1, "time": 95.2175}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 17.4877}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 31.5802}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 40.39}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 57.8297}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 90.1407}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 128, "hpad": 1, "wpad": 1, "time": 152.749}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 32.2842}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 79.4879}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 75.3827}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 101.311}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 155.229}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 3, "kw": 3,"oh":96, "ow": 96, "ofm": 256, "hpad": 1, "wpad": 1, "time": 265.557}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 11.8567}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 8.70085}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 18.7147}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 30.8741}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 54.4315}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 8, "hpad": 1, "wpad": 1, "time": 112.323}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 12.1391}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 11.2731}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 19.9158}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 31.4882}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 57.5107}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 16, "hpad": 1, "wpad": 1, "time": 109.722}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 15.0636}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 17.9313}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 28.5085}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 41.7776}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 71.7545}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 32, "hpad": 1, "wpad": 1, "time": 136.688}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 20.3307}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 32.1066}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 43.6803}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 62.4708}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 102.554}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 64, "hpad": 1, "wpad": 1, "time": 184.284}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 30.5306}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 58.9274}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 78.2267}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 105.893}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 160.784}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 128, "hpad": 1, "wpad": 1, "time": 287.782}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 53.5964}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 151.081}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 142.725}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 193.157}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 293.429}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 3, "kw": 3,"oh":128, "ow": 128, "ofm": 256, "hpad": 1, "wpad": 1, "time": 501.576}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 35.9982}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 22.99}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 47.862}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 76.7335}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 141.974}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 8, "hpad": 1, "wpad": 1, "time": 290.001}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 32.5944}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 32.5106}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 50.7017}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 81.9141}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 145.198}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 16, "hpad": 1, "wpad": 1, "time": 290.466}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 41.0356}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 47.4174}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 75.5906}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 109.527}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 189.14}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 32, "hpad": 1, "wpad": 1, "time": 361.471}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 57.4268}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 85.0909}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 114.687}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 163.797}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 277.23}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 64, "hpad": 1, "wpad": 1, "time": 510.343}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 88.4214}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 156.786}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 203.763}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 283.106}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 457.971}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 128, "hpad": 1, "wpad": 1, "time": 819.4}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 165.673}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 415.677}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 384.831}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 531.277}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 840.485}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 3, "kw": 3,"oh":224, "ow": 224, "ofm": 256, "hpad": 1, "wpad": 1, "time": 1470.46}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 8, "hpad": 2, "wpad": 2, "time": 0.422381}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 8, "hpad": 2, "wpad": 2, "time": 1.01275}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 8, "hpad": 2, "wpad": 2, "time": 2.29943}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 8, "hpad": 2, "wpad": 2, "time": 3.68331}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 8, "hpad": 2, "wpad": 2, "time": 5.4816}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 8, "hpad": 2, "wpad": 2, "time": 8.38233}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 16, "hpad": 2, "wpad": 2, "time": 0.626681}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 16, "hpad": 2, "wpad": 2, "time": 1.46484}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 16, "hpad": 2, "wpad": 2, "time": 3.09368}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 16, "hpad": 2, "wpad": 2, "time": 4.8867}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 16, "hpad": 2, "wpad": 2, "time": 7.51082}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 16, "hpad": 2, "wpad": 2, "time": 10.6508}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 32, "hpad": 2, "wpad": 2, "time": 0.706671}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 32, "hpad": 2, "wpad": 2, "time": 1.65233}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 32, "hpad": 2, "wpad": 2, "time": 4.23055}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 32, "hpad": 2, "wpad": 2, "time": 5.92301}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 32, "hpad": 2, "wpad": 2, "time": 8.25947}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 32, "hpad": 2, "wpad": 2, "time": 13.0512}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 64, "hpad": 2, "wpad": 2, "time": 0.864808}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 64, "hpad": 2, "wpad": 2, "time": 2.43303}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 64, "hpad": 2, "wpad": 2, "time": 4.7789}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 64, "hpad": 2, "wpad": 2, "time": 6.92286}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 64, "hpad": 2, "wpad": 2, "time": 10.57}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 64, "hpad": 2, "wpad": 2, "time": 18.2988}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1.3137}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 128, "hpad": 2, "wpad": 2, "time": 3.61389}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 128, "hpad": 2, "wpad": 2, "time": 6.25867}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 128, "hpad": 2, "wpad": 2, "time": 9.41436}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 128, "hpad": 2, "wpad": 2, "time": 16.5246}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 128, "hpad": 2, "wpad": 2, "time": 29.1654}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 256, "hpad": 2, "wpad": 2, "time": 2.34492}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 256, "hpad": 2, "wpad": 2, "time": 5.76951}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 256, "hpad": 2, "wpad": 2, "time": 9.35426}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 256, "hpad": 2, "wpad": 2, "time": 15.168}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 256, "hpad": 2, "wpad": 2, "time": 27.6709}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 5, "kw": 5,"oh":16, "ow": 16, "ofm": 256, "hpad": 2, "wpad": 2, "time": 50.7188}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 1.12849}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 2.97059}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 6.11946}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 8.42006}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 14.0249}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 8, "hpad": 2, "wpad": 2, "time": 26.5804}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 1.40115}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 4.11876}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 6.75441}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 10.7182}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 18.4082}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 16, "hpad": 2, "wpad": 2, "time": 35.3744}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 1.75181}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 5.18051}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 8.03829}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 12.8429}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 23.4298}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 32, "hpad": 2, "wpad": 2, "time": 46.5167}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 2.86378}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 7.31212}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 11.1457}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 19.9197}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 38.2378}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 64, "hpad": 2, "wpad": 2, "time": 72.9878}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 4.77099}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 9.99565}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 17.6272}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 32.0773}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 61.5932}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 128, "hpad": 2, "wpad": 2, "time": 120.922}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 8.33598}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 17.3583}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 31.1464}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 59.347}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 115.721}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 5, "kw": 5,"oh":32, "ow": 32, "ofm": 256, "hpad": 2, "wpad": 2, "time": 221.746}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 4.8533}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 10.759}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 16.7719}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 28.9584}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 55.0821}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 8, "hpad": 2, "wpad": 2, "time": 102.378}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 4.52517}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 12.5797}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 20.9785}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 38.0046}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 70.8516}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 16, "hpad": 2, "wpad": 2, "time": 138.149}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 6.29167}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 15.9441}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 27.2043}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 48.7481}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 97.0882}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 32, "hpad": 2, "wpad": 2, "time": 185.798}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 9.4112}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 22.2797}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 42.4038}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 79.0232}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 153.331}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 64, "hpad": 2, "wpad": 2, "time": 299.265}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 15.9509}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 36.9004}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 66.0444}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 126.954}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 248.241}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 128, "hpad": 2, "wpad": 2, "time": 496.591}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 29.3859}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 65.1059}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 126.14}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 243.785}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 477.264}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 5, "kw": 5,"oh":64, "ow": 64, "ofm": 256, "hpad": 2, "wpad": 2, "time": 934.649}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 10.3469}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 21.56}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 34.3801}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 62.1749}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 114.513}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 8, "hpad": 2, "wpad": 2, "time": 231.135}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 9.56028}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 26.4462}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 43.9353}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 81.5361}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 153.413}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 16, "hpad": 2, "wpad": 2, "time": 309.084}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 13.2491}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 33.1144}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 58.3609}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 104.481}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 207.469}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 32, "hpad": 2, "wpad": 2, "time": 422.156}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 21.4733}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 50.4406}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 93.8485}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 173.154}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 339.65}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 64, "hpad": 2, "wpad": 2, "time": 679.281}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 37.0229}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 82.1462}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 150.117}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 284.205}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 579.961}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1143.68}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 69.1879}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 140.401}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 271.991}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 533.992}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1081.3}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 5, "kw": 5,"oh":96, "ow": 96, "ofm": 256, "hpad": 2, "wpad": 2, "time": 2156.2}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 18.1421}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 37.4486}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 64.9342}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 106.336}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 206.373}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 8, "hpad": 2, "wpad": 2, "time": 415.829}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 18.358}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 42.7156}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 77.4014}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 140.714}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 276.398}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 16, "hpad": 2, "wpad": 2, "time": 567.458}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 24.5139}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 57.7053}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 104.409}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 193.201}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 380.899}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 32, "hpad": 2, "wpad": 2, "time": 762.674}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 37.9478}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 85.0549}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 158.479}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 302.356}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 615.035}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 64, "hpad": 2, "wpad": 2, "time": 1194.22}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 66.5413}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 139.564}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 264.029}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 524.485}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1038.21}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 128, "hpad": 2, "wpad": 2, "time": 2066}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 122.712}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 251.801}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 488.49}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 973.721}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1941.96}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 5, "kw": 5,"oh":128, "ow": 128, "ofm": 256, "hpad": 2, "wpad": 2, "time": 3846.66}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 55.1101}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 106.62}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 180.931}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 321.771}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 638.812}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 8, "hpad": 2, "wpad": 2, "time": 1296.82}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 53.791}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 133.05}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 236.066}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 427.394}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 865.621}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 16, "hpad": 2, "wpad": 2, "time": 1739.92}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 74.8882}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 172.349}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 308.759}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 590.924}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 1181.79}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 32, "hpad": 2, "wpad": 2, "time": 2351.76}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 116.393}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 265.931}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 485.93}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 947.993}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 1887.24}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 64, "hpad": 2, "wpad": 2, "time": 3737.24}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 205.203}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 430.607}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 811.856}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 1628.52}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 3225.2}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 128, "hpad": 2, "wpad": 2, "time": 6375.58}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 380.539}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 777.063}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 1534.95}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 3001.46}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 6116.8}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 5, "kw": 5,"oh":224, "ow": 224, "ofm": 256, "hpad": 2, "wpad": 2, "time": 11986.9}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 8, "hpad": 3, "wpad": 3, "time": 0.458196}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 8, "hpad": 3, "wpad": 3, "time": 0.654015}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 8, "hpad": 3, "wpad": 3, "time": 1.07632}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 8, "hpad": 3, "wpad": 3, "time": 2.27649}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 8, "hpad": 3, "wpad": 3, "time": 4.69983}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 8, "hpad": 3, "wpad": 3, "time": 7.29934}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 16, "hpad": 3, "wpad": 3, "time": 0.643475}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 16, "hpad": 3, "wpad": 3, "time": 1.53591}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 16, "hpad": 3, "wpad": 3, "time": 3.50736}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 16, "hpad": 3, "wpad": 3, "time": 5.62985}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 16, "hpad": 3, "wpad": 3, "time": 8.54507}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 16, "hpad": 3, "wpad": 3, "time": 12.2564}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 32, "hpad": 3, "wpad": 3, "time": 0.949348}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 32, "hpad": 3, "wpad": 3, "time": 1.93792}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 32, "hpad": 3, "wpad": 3, "time": 4.31003}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 32, "hpad": 3, "wpad": 3, "time": 6.83056}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 32, "hpad": 3, "wpad": 3, "time": 9.911}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 32, "hpad": 3, "wpad": 3, "time": 15.3037}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 64, "hpad": 3, "wpad": 3, "time": 1.19282}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 64, "hpad": 3, "wpad": 3, "time": 2.84754}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 64, "hpad": 3, "wpad": 3, "time": 5.82278}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 64, "hpad": 3, "wpad": 3, "time": 8.81377}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 64, "hpad": 3, "wpad": 3, "time": 13.2444}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 64, "hpad": 3, "wpad": 3, "time": 25.9574}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 128, "hpad": 3, "wpad": 3, "time": 2.12073}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 128, "hpad": 3, "wpad": 3, "time": 4.37343}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 128, "hpad": 3, "wpad": 3, "time": 8.43925}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 128, "hpad": 3, "wpad": 3, "time": 12.7593}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 128, "hpad": 3, "wpad": 3, "time": 20.6792}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 128, "hpad": 3, "wpad": 3, "time": 40.83}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 8, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 256, "hpad": 3, "wpad": 3, "time": 2.52307}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 16, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 256, "hpad": 3, "wpad": 3, "time": 6.62725}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 32, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 256, "hpad": 3, "wpad": 3, "time": 11.1448}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 64, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 256, "hpad": 3, "wpad": 3, "time": 21.1892}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 128, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 256, "hpad": 3, "wpad": 3, "time": 37.3074}
,
{"op": "conv", "ih": 16, "iw": 16, "ch": 256, "kh": 7, "kw": 7,"oh":16, "ow": 16, "ofm": 256, "hpad": 3, "wpad": 3, "time": 69.5741}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 8, "hpad": 3, "wpad": 3, "time": 2.03913}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 8, "hpad": 3, "wpad": 3, "time": 4.09802}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 8, "hpad": 3, "wpad": 3, "time": 6.87722}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 8, "hpad": 3, "wpad": 3, "time": 12.2685}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 8, "hpad": 3, "wpad": 3, "time": 21.8593}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 8, "hpad": 3, "wpad": 3, "time": 40.9964}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 16, "hpad": 3, "wpad": 3, "time": 3.1432}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 16, "hpad": 3, "wpad": 3, "time": 5.92478}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 16, "hpad": 3, "wpad": 3, "time": 9.33266}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 16, "hpad": 3, "wpad": 3, "time": 15.9365}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 16, "hpad": 3, "wpad": 3, "time": 29.7054}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 16, "hpad": 3, "wpad": 3, "time": 55.2523}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 32, "hpad": 3, "wpad": 3, "time": 4.029}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 32, "hpad": 3, "wpad": 3, "time": 7.05061}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 32, "hpad": 3, "wpad": 3, "time": 11.4698}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 32, "hpad": 3, "wpad": 3, "time": 20.7955}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 32, "hpad": 3, "wpad": 3, "time": 38.0133}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 32, "hpad": 3, "wpad": 3, "time": 78.26}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 64, "hpad": 3, "wpad": 3, "time": 5.43374}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 64, "hpad": 3, "wpad": 3, "time": 9.50014}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 64, "hpad": 3, "wpad": 3, "time": 17.0552}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 64, "hpad": 3, "wpad": 3, "time": 32.2634}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 64, "hpad": 3, "wpad": 3, "time": 60.8203}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 64, "hpad": 3, "wpad": 3, "time": 120.887}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 128, "hpad": 3, "wpad": 3, "time": 7.38131}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 128, "hpad": 3, "wpad": 3, "time": 15.0916}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 128, "hpad": 3, "wpad": 3, "time": 27.8973}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 128, "hpad": 3, "wpad": 3, "time": 53.2858}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 128, "hpad": 3, "wpad": 3, "time": 99.8877}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 128, "hpad": 3, "wpad": 3, "time": 197.886}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 8, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 256, "hpad": 3, "wpad": 3, "time": 12.6885}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 16, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 256, "hpad": 3, "wpad": 3, "time": 26.0962}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 32, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 256, "hpad": 3, "wpad": 3, "time": 49.1719}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 64, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 256, "hpad": 3, "wpad": 3, "time": 96.3429}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 128, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 256, "hpad": 3, "wpad": 3, "time": 184.377}
,
{"op": "conv", "ih": 32, "iw": 32, "ch": 256, "kh": 7, "kw": 7,"oh":32, "ow": 32, "ofm": 256, "hpad": 3, "wpad": 3, "time": 372.941}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 8, "hpad": 3, "wpad": 3, "time": 8.38597}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 8, "hpad": 3, "wpad": 3, "time": 15.2996}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 8, "hpad": 3, "wpad": 3, "time": 26.1195}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 8, "hpad": 3, "wpad": 3, "time": 50.7439}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 8, "hpad": 3, "wpad": 3, "time": 92.434}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 8, "hpad": 3, "wpad": 3, "time": 186.126}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 16, "hpad": 3, "wpad": 3, "time": 10.2539}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 16, "hpad": 3, "wpad": 3, "time": 19.2486}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 16, "hpad": 3, "wpad": 3, "time": 36.1659}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 16, "hpad": 3, "wpad": 3, "time": 65.7914}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 16, "hpad": 3, "wpad": 3, "time": 124.691}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 16, "hpad": 3, "wpad": 3, "time": 250.035}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 32, "hpad": 3, "wpad": 3, "time": 13.0261}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 32, "hpad": 3, "wpad": 3, "time": 25.0766}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 32, "hpad": 3, "wpad": 3, "time": 45.766}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 32, "hpad": 3, "wpad": 3, "time": 88.505}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 32, "hpad": 3, "wpad": 3, "time": 166.578}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 32, "hpad": 3, "wpad": 3, "time": 338.387}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 64, "hpad": 3, "wpad": 3, "time": 19.9746}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 64, "hpad": 3, "wpad": 3, "time": 39.4069}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 64, "hpad": 3, "wpad": 3, "time": 71.8846}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 64, "hpad": 3, "wpad": 3, "time": 140.517}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 64, "hpad": 3, "wpad": 3, "time": 273.951}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 64, "hpad": 3, "wpad": 3, "time": 554.203}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 128, "hpad": 3, "wpad": 3, "time": 32.3727}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 128, "hpad": 3, "wpad": 3, "time": 60.8552}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 128, "hpad": 3, "wpad": 3, "time": 118.287}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 128, "hpad": 3, "wpad": 3, "time": 233.092}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 128, "hpad": 3, "wpad": 3, "time": 462.671}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 128, "hpad": 3, "wpad": 3, "time": 921.076}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 8, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 256, "hpad": 3, "wpad": 3, "time": 58.7364}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 16, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 256, "hpad": 3, "wpad": 3, "time": 113.938}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 32, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 256, "hpad": 3, "wpad": 3, "time": 221.855}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 64, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 256, "hpad": 3, "wpad": 3, "time": 430.542}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 128, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 256, "hpad": 3, "wpad": 3, "time": 858.274}
,
{"op": "conv", "ih": 64, "iw": 64, "ch": 256, "kh": 7, "kw": 7,"oh":64, "ow": 64, "ofm": 256, "hpad": 3, "wpad": 3, "time": 1745.01}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 8, "hpad": 3, "wpad": 3, "time": 19.2451}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 8, "hpad": 3, "wpad": 3, "time": 34.2535}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 8, "hpad": 3, "wpad": 3, "time": 58.3658}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 8, "hpad": 3, "wpad": 3, "time": 110.213}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 8, "hpad": 3, "wpad": 3, "time": 216.429}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 8, "hpad": 3, "wpad": 3, "time": 432.364}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 16, "hpad": 3, "wpad": 3, "time": 22.9957}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 16, "hpad": 3, "wpad": 3, "time": 40.3599}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 16, "hpad": 3, "wpad": 3, "time": 74.7055}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 16, "hpad": 3, "wpad": 3, "time": 145.078}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 16, "hpad": 3, "wpad": 3, "time": 286.123}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 16, "hpad": 3, "wpad": 3, "time": 590.387}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 32, "hpad": 3, "wpad": 3, "time": 30.662}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 32, "hpad": 3, "wpad": 3, "time": 55.0406}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 32, "hpad": 3, "wpad": 3, "time": 103.388}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 32, "hpad": 3, "wpad": 3, "time": 202.843}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 32, "hpad": 3, "wpad": 3, "time": 402.91}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 32, "hpad": 3, "wpad": 3, "time": 792.001}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 64, "hpad": 3, "wpad": 3, "time": 45.5453}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 64, "hpad": 3, "wpad": 3, "time": 84.5776}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 64, "hpad": 3, "wpad": 3, "time": 159.845}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 64, "hpad": 3, "wpad": 3, "time": 318.548}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 64, "hpad": 3, "wpad": 3, "time": 628.443}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 64, "hpad": 3, "wpad": 3, "time": 1277.81}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 128, "hpad": 3, "wpad": 3, "time": 74.4757}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 128, "hpad": 3, "wpad": 3, "time": 139.974}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 128, "hpad": 3, "wpad": 3, "time": 269.221}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 128, "hpad": 3, "wpad": 3, "time": 535.507}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 128, "hpad": 3, "wpad": 3, "time": 1071.37}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 128, "hpad": 3, "wpad": 3, "time": 2154.43}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 8, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 256, "hpad": 3, "wpad": 3, "time": 136.756}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 16, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 256, "hpad": 3, "wpad": 3, "time": 265.351}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 32, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 256, "hpad": 3, "wpad": 3, "time": 519.455}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 64, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 256, "hpad": 3, "wpad": 3, "time": 1019.73}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 128, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 256, "hpad": 3, "wpad": 3, "time": 2003.09}
,
{"op": "conv", "ih": 96, "iw": 96, "ch": 256, "kh": 7, "kw": 7,"oh":96, "ow": 96, "ofm": 256, "hpad": 3, "wpad": 3, "time": 4056.13}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 8, "hpad": 3, "wpad": 3, "time": 34.0501}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 8, "hpad": 3, "wpad": 3, "time": 57.6356}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 8, "hpad": 3, "wpad": 3, "time": 103.449}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 8, "hpad": 3, "wpad": 3, "time": 197.177}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 8, "hpad": 3, "wpad": 3, "time": 385.988}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 8, "hpad": 3, "wpad": 3, "time": 786.34}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 16, "hpad": 3, "wpad": 3, "time": 42.0826}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 16, "hpad": 3, "wpad": 3, "time": 74.9845}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 16, "hpad": 3, "wpad": 3, "time": 139.112}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 16, "hpad": 3, "wpad": 3, "time": 268.189}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 16, "hpad": 3, "wpad": 3, "time": 517.483}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 16, "hpad": 3, "wpad": 3, "time": 1065.35}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 32, "hpad": 3, "wpad": 3, "time": 55.1252}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 32, "hpad": 3, "wpad": 3, "time": 99.8929}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 32, "hpad": 3, "wpad": 3, "time": 189.11}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 32, "hpad": 3, "wpad": 3, "time": 369.03}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 32, "hpad": 3, "wpad": 3, "time": 710.826}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 32, "hpad": 3, "wpad": 3, "time": 1451.53}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 64, "hpad": 3, "wpad": 3, "time": 83.7191}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 64, "hpad": 3, "wpad": 3, "time": 156.974}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 64, "hpad": 3, "wpad": 3, "time": 300.1}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 64, "hpad": 3, "wpad": 3, "time": 576.847}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 64, "hpad": 3, "wpad": 3, "time": 1136.65}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 64, "hpad": 3, "wpad": 3, "time": 2331.97}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 128, "hpad": 3, "wpad": 3, "time": 135.415}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 128, "hpad": 3, "wpad": 3, "time": 254.728}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 128, "hpad": 3, "wpad": 3, "time": 506.538}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 128, "hpad": 3, "wpad": 3, "time": 1005.04}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 128, "hpad": 3, "wpad": 3, "time": 1921.97}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 128, "hpad": 3, "wpad": 3, "time": 3953.95}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 8, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 256, "hpad": 3, "wpad": 3, "time": 241.396}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 16, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 256, "hpad": 3, "wpad": 3, "time": 471.234}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 32, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 256, "hpad": 3, "wpad": 3, "time": 935.003}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 64, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 256, "hpad": 3, "wpad": 3, "time": 1847.42}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 128, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 256, "hpad": 3, "wpad": 3, "time": 3661.38}
,
{"op": "conv", "ih": 128, "iw": 128, "ch": 256, "kh": 7, "kw": 7,"oh":128, "ow": 128, "ofm": 256, "hpad": 3, "wpad": 3, "time": 7413.09}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 8, "hpad": 3, "wpad": 3, "time": 105.814}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 8, "hpad": 3, "wpad": 3, "time": 178.341}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 8, "hpad": 3, "wpad": 3, "time": 311.939}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 8, "hpad": 3, "wpad": 3, "time": 617.597}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 8, "hpad": 3, "wpad": 3, "time": 1215.57}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 8, "hpad": 3, "wpad": 3, "time": 2481.06}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 16, "hpad": 3, "wpad": 3, "time": 130.892}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 16, "hpad": 3, "wpad": 3, "time": 232.604}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 16, "hpad": 3, "wpad": 3, "time": 419.362}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 16, "hpad": 3, "wpad": 3, "time": 825.438}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 16, "hpad": 3, "wpad": 3, "time": 1645.96}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 16, "hpad": 3, "wpad": 3, "time": 3353.4}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 32, "hpad": 3, "wpad": 3, "time": 170.312}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 32, "hpad": 3, "wpad": 3, "time": 303.985}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 32, "hpad": 3, "wpad": 3, "time": 569.641}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 32, "hpad": 3, "wpad": 3, "time": 1132.23}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 32, "hpad": 3, "wpad": 3, "time": 2249.29}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 32, "hpad": 3, "wpad": 3, "time": 4573.03}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 64, "hpad": 3, "wpad": 3, "time": 256.462}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 64, "hpad": 3, "wpad": 3, "time": 472.864}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 64, "hpad": 3, "wpad": 3, "time": 910.65}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 64, "hpad": 3, "wpad": 3, "time": 1831.27}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 64, "hpad": 3, "wpad": 3, "time": 3608.21}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 64, "hpad": 3, "wpad": 3, "time": 7254.84}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 128, "hpad": 3, "wpad": 3, "time": 428.622}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 128, "hpad": 3, "wpad": 3, "time": 793.074}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 128, "hpad": 3, "wpad": 3, "time": 1552.57}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 128, "hpad": 3, "wpad": 3, "time": 3090.32}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 128, "hpad": 3, "wpad": 3, "time": 6079.66}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 128, "hpad": 3, "wpad": 3, "time": 12367}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 8, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 256, "hpad": 3, "wpad": 3, "time": 756.878}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 16, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 256, "hpad": 3, "wpad": 3, "time": 1491.97}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 32, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 256, "hpad": 3, "wpad": 3, "time": 2909.08}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 64, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 256, "hpad": 3, "wpad": 3, "time": 5879.12}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 128, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 256, "hpad": 3, "wpad": 3, "time": 11524.7}
,
{"op": "conv", "ih": 224, "iw": 224, "ch": 256, "kh": 7, "kw": 7,"oh":224, "ow": 224, "ofm": 256, "hpad": 3, "wpad": 3, "time": 23247.5}
]
