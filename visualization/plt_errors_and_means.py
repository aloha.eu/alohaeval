import matplotlib.pyplot as plt
import numpy as np


def show_means_and_errors_multi_bar(estimation_errors_per_method, method_names):
    """
    Show means and errors graph for eval. error per method
    :param estimation_errors_per_method: list of arrays, where every array shows errors for
    an evaluation method, and every array element is an evaluation error
    :param method_names: names of evaluation methods
    """
    bar_names = method_names
    # Calculate the average (mean)
    mean = [np.mean(estimation_error) for estimation_error in estimation_errors_per_method]

    # Calculate the standard deviation
    std = [np.std(estimation_error) for estimation_error in estimation_errors_per_method]

    # Create lists for the plot
    x_pos = np.arange(len(bar_names))

    # Build the plot
    fig, ax = plt.subplots()
    ax.bar(x_pos, mean, yerr=std, align='center', alpha=0.5,  color="grey", ecolor='black', capsize=10)
    ax.set_ylabel('latency estimation error, %')
    ax.set_xticks(x_pos)
    ax.set_xticklabels(bar_names)
    # ax.set_title('Coefficent of Thermal Expansion (CTE) of Three Metals')
    ax.yaxis.grid(True)

    # Save the figure and show
    plt.tight_layout()
    plt.show()


def show_means_and_errors_one_bar(estimation_errors, method_name):
    """
    Show means and errors graph for eval. errors array
    :param estimation_errors: errors array, which shows errors for
    an evaluation method, with every array element being an evaluation error
    :param method_name: name of the evaluation method
    """
    bar_names = [method_name]

    # Calculate the average (mean)
    mean = np.mean(estimation_errors)

    # Calculate the standard deviation
    std = np.std(estimation_errors)

    # Create lists for the plot
    x_pos = np.arange(len(bar_names))

    # Build the plot
    fig, ax = plt.subplots()
    ax.bar(x_pos, [mean], yerr=[std], align='center', alpha=0.5, color="grey", ecolor='black', capsize=10)
    ax.set_ylabel('latency estimation error, %')
    ax.set_xticks(x_pos)
    ax.set_xticklabels(bar_names)
    # ax.set_title('Coefficent of Thermal Expansion (CTE) of Three Metals')
    ax.yaxis.grid(True)

    # Save the figure and show
    plt.tight_layout()
    plt.show()


def test():
    moc_errors1 = [10, 20, 0, 0, 5, 15, 6]
    moc_errors2 = [5, 15, 0, 17, 2, 12, 3]
    show_means_and_errors_multi_bar([moc_errors1, moc_errors2], ["moc error 1", "moc error 2"])

