import numpy as np
import matplotlib.pyplot as plt


def point_ids_OX(len):
    """
    Generate OX-axis for plot where every xi in OX is an id of a pgraph point
    """
    point_ids = []
    for pid in range(0, len):
        point_ids.append(pid)
    return point_ids


def show_plot(x, y, x_label, y_label):
    """
    Show scatter plot with one graph y =f (x)
    :param x values [x1,x2,...,xn] along OX axis
    :param y values [y1,y2,...,yn] along OY axis, where y1 = f(x1), y2= f(x2)...yn = f(xn)
    :param x label - label for x-axis
    :param y_label - label for y-axis
    """

    x0 = np.min(x)
    x1 = np.max(x)

    y0 = np.min(y)
    y1 = np.max(y)

    plt.plot(x, y, '.b-')
    plt.axis([x0, x1, y0, y1])
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()


def show_scatter_plot_multi_y(x, y_list, x_label, y_label):
    """
    Show scatter plot with several graphs, sharing the same OX axis y = f (x), y' = f'(x), y'' = f''(x)...
    :param x values [x1,x2,...,xn] along OX axis
    :param y_list array with OX values [y, y', y''....], where
         y values [y1,y2,...,yn] along OY axis, where y1 = f(x1), y2= f(x2)...yn = f(xn)
         y' values [y'1,y'2,...,y'n] along OY axis, where y'1 = f'(x1), y'2= f'(x2)...y'n = f'(xn)
         ...
    :param x label - label for x-axis
    :param y_label - label for y-axis
    """
    x0 = np.min(x)
    x1 = np.max(x)

    y0 = np.min(y_list[0])
    y1 = np.max(y_list[0])

    colors = []
    color = get_color_scatter(0)
    colors.append(color)

    for i in range(1, y_list.__len__()):
        y0 = min(y0, np.min(y_list[i]))
        y1 = max(y1, np.max(y_list[i]))
        color = get_color_scatter(i)
        colors.append(color)


    for i in range(0, y_list.__len__()):
        plt.plot(x, y_list[i], colors[i])#, alpha=0.5)

    plt.axis([x0, x1, y0, y1])
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()



def show_scatter_plot_roofline(intencities, roofline_points, measurement_points, max_intencity = -1):
    """
    Show scatter plot with several graphs, sharing the same OX axis y = f (x), y' = f'(x), y'' = f''(x)...
    :param x values [x1,x2,...,xn] along OX axis
    :param y_list array with OX values [y, y', y''....], where
         y values [y1,y2,...,yn] along OY axis, where y1 = f(x1), y2= f(x2)...yn = f(xn)
         y' values [y'1,y'2,...,y'n] along OY axis, where y'1 = f'(x1), y'2= f'(x2)...y'n = f'(xn)
         ...
    :param x label - label for x-axis
    :param y_label - label for y-axis
    """
    x0 = np.min(intencities)
    x1 = np.max(intencities)

    y0 = min(np.min(roofline_points), np.min(measurement_points))
    y1 = max(np.max(roofline_points), np.max(measurement_points)) + 7

    [intersection_intencity, intersection_performance] = find_intersection_point(intencities, roofline_points)
    shrinked_roofline_intencities = [x0, intersection_intencity, x1]
    shrinked_roofline_points = [ np.min(roofline_points), intersection_performance, np.max(roofline_points)]

    color_roofline = '.k-'

    if max_intencity < 0:
        plt.plot(shrinked_roofline_intencities, shrinked_roofline_points, color_roofline, lw=3)#, alpha=0.5)
        plt.plot(intencities, measurement_points, marker="o", color = "#3d7cff", linestyle = "None", alpha=0.5)
    else:
        max_intencity_id = find_intecity_id(intencities, max_intencity)
        x1 = min(x1, max_intencity)
        y1 = max(roofline_points[max_intencity_id], measurement_points[max_intencity_id]) + 7
        [intersection_intencity, intersection_performance] = find_intersection_point(intencities, roofline_points)
        shrinked_roofline_intencities = [x0, intersection_intencity, x1]
        shrinked_roofline_points = [np.min(roofline_points), intersection_performance, np.max(roofline_points)]

        plt.plot(shrinked_roofline_intencities, shrinked_roofline_points, color_roofline, lw=3)#, alpha=0.5)
        plt.plot(intencities, measurement_points, marker="o", color = "#3d7cff", linestyle = "None", alpha=0.5)



    plt.axis([x0, x1, y0, y1])
    plt.xlabel("FLOPS/Byte")
    plt.ylabel("GOPS/s")

    plt.annotate(xy=[300, 610], s= "Roofline")
    plt.annotate(xy=[200, 500], s="Kernels")
    plt.show()


def find_intecity_id(intencities, intencity):
    max_intencity = np.max(intencities)
    if intencity >= max_intencity:
        return len(intencities)-1
    for id in range(len(intencities)-1):
        if intencities[id] >= intencity:
            return id


def find_intersection_point(intencities, roofline_points):
    y_max = np.max(roofline_points)
    for point_id in range(len(roofline_points)-1):
        if roofline_points[point_id] == y_max:
            return [intencities[point_id], roofline_points[point_id]]


def show_plot_multi_y(x, y_list, x_label, y_label):
    """
    Show scatter plot with several graphs, sharing the same OX axis y = f (x), y' = f'(x), y'' = f''(x)...
    :param x values [x1,x2,...,xn] along OX axis
    :param y_list array with OX values [y, y', y''....], where
         y values [y1,y2,...,yn] along OY axis, where y1 = f(x1), y2= f(x2)...yn = f(xn)
         y' values [y'1,y'2,...,y'n] along OY axis, where y'1 = f'(x1), y'2= f'(x2)...y'n = f'(xn)
         ...
    :param x label - label for x-axis
    :param y_label - label for y-axis
    """
    x0 = np.min(x)
    x1 = np.max(x)

    y0 = np.min(y_list[0])
    y1 = np.max(y_list[0])

    colors = [ ]
    color = get_color(0)
    colors.append(color)

    for i in range(1, y_list.__len__()):
        y0 = min(y0, np.min(y_list[i]))
        y1 = max(y1, np.max(y_list[i]))
        color = get_color(i)
        colors.append(color)

    for i in range(0, y_list.__len__()):
        plt.plot(x, y_list[i], colors[i])

    plt.axis([x0, x1, y0, y1])
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()


def get_color(graph_id):
    """
    Get graph plot color - made to distinguish several graphs on one plot
    :param graph_id - id of the graph in graph list
    """
    colors = ['.r-', '.b-', '.m-', '.c-']
    colors_num = colors.__len__()
    color_id = graph_id % colors_num
    return colors[color_id]


def get_color_scatter(graph_id):
    """
    Get graph plot color - made to distinguish several graphs on one plot
    :param graph_id - id of the graph in graph list
    """
    colors = ['.r', '.b', '.m', '.c']
    colors_num = colors.__len__()
    color_id = graph_id % colors_num
    return colors[color_id]
