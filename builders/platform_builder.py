"""
This is an API module for building common platform specification for
Neuraghe and Jetson platforms
"""
from util import giga
from platform_spec.platform_spec import EmbeddedPlatform, Processor, Memory, DataTransferChannel, SharedResource
from platform_spec.comp_model import DimsUnrolling


def build_neuraghe_specification():
    neuraghe = EmbeddedPlatform("Neuraghe")
    # add common parameters
    neuraghe.data_bytes = 2
    neuraghe.total_bandwidth = 3.6
    neuraghe.memory_partitions = 2

    # add global memories and input_examples transfer channels

    # add main (off-chip) memory
    main_mem = Memory("main", -1)
    neuraghe.memories.append(main_mem)

    main_to_main_data_channel = DataTransferChannel("main", "main", -1)
    neuraghe.channels.append(main_to_main_data_channel)

    # add processors and local memories
    ##################################
    #       FPGA (Engine)            #
    engine = Processor("Engine", "Engine")

    # parallel comp. matrix here
    engine.parallel_comp_matrix = [4, 9, 10]

    # common properties here
    engine.max_perf = 129.6
    engine.frequency = 0.18 # GHz

    # memory mapping here
    engine.data_on_memory_mapping = {
        "input_data": "input_memory",
        "output_data": "output_memory",
        "weights": "weights_memory"
    }

    # additional shared resources here

    # add local memories
    weight_memory = Memory("weights_memory", 184320)
    neuraghe.memories.append(weight_memory)
    input_memory = Memory("input_memory", 9 * 16384)
    neuraghe.memories.append(input_memory)
    output_memory = Memory("output_memory", 20 * 16384, 1638)
    neuraghe.memories.append(output_memory)

    # add local input_examples transfer channels
    i_data_channel = DataTransferChannel("main", "input_memory", 0.72)
    o_data_channel = DataTransferChannel("main", "output_memory", 0.72)
    w_channel = DataTransferChannel("main", "weights_memory", 2.88)
    neuraghe.channels.append(i_data_channel)
    neuraghe.channels.append(o_data_channel)
    neuraghe.channels.append(w_channel)

    # add additional resources

    # unrolling (comp. model)
    engine.unrolling = create_neuraghe_engine_comp_model(neuraghe, engine)

    # add processor to the platform
    neuraghe.processors.append(engine)

    return neuraghe


def create_neuraghe_engine_comp_model(platform, processor):
    unrolling = DimsUnrolling()
    unrolling.loops_order = [2, 3, 0, 1, 4, 5]
    unrolling.add_computations_unrolling(["ow"], [0])
    unrolling.add_computations_unrolling(["ifm"], [1])
    unrolling.add_computations_unrolling(["ofm"], [2])

    # engine.parallel_comp_matrix = [4, 9, 10]

    inp_data_memory_name = processor.data_on_memory_mapping["input_data"]
    inp_data_memory = platform.get_memory(inp_data_memory_name)

    outp_data_memory_name = processor.data_on_memory_mapping["output_data"]
    outp_data_memory = platform.get_memory(outp_data_memory_name)

    w_data_memory_name = processor.data_on_memory_mapping["weights"]
    w_data_memory = platform.get_memory(w_data_memory_name)

    unrolling.add_limit(resource_user_name="output_data", resource=outp_data_memory,
                        dims=["ofm", "ow", "oh"], split_dims=["ofm"], load_dim="ifm", load=False)

    unrolling.add_limit(resource_user_name="weights", resource=w_data_memory,
                        dims=["ofm", "ifm", "kh", "kw"], split_dims=["ifm"], load_dim="parallel1", load=True)

    unrolling.add_limit(resource_user_name="input_data", resource=inp_data_memory,
                        dims=["ifm", "ow", "oh"], split_dims=["ifm"], load_dim="parallel1", load=True)

    return unrolling


def build_jetson_specification():
    jetson = EmbeddedPlatform("Jetson")
    # add common parameters
    jetson.data_bytes = 4
    jetson.total_bandwidth = 59
    jetson.memory_partitions = 2

    # add global memories and input_examples transfer channels

    # add main memory
    main_mem = Memory("main", 8 * giga() * 4)
    jetson.memories.append(main_mem)

    # add shared l2 cache
    l2_cache = Memory("l2_cache", 512000, 4) # 204800
    jetson.memories.append(l2_cache)

    main_to_main_data_channel = DataTransferChannel("main", "main", -1)
    jetson.channels.append(main_to_main_data_channel)

    i_data_channel = DataTransferChannel("main", "l2_cache", 12)
    jetson.channels.append(i_data_channel)

    # add processors and local memories
    ##################################
    #              GPU              #
    for i in range(0, 1):
        gpu = Processor("GPU" + str(i), "GPU")

        # parallel comp. matrix here
        gpu.parallel_comp_matrix = [256, 16, 1]
        # add warps to parallelism levels?
        # jetson.threads_per_warp = 32

        # common properties here
        gpu.max_perf = 667
        gpu.max_power = 15 # Watt
        gpu.frequency = 1.3 #GHz

        # memory mapping here
        gpu.data_on_memory_mapping = {
            "input_data": "sh_mem_GPU" + str(i),
            "output_data": "main",
            "weights": "main"
        }

        # add local memories
        sh_memory = Memory("sh_mem_GPU" + str(i), 65536 * 2, 4)
        jetson.memories.append(sh_memory)

        # add local input_examples transfer channels
        i_data_channel = DataTransferChannel("main", "sh_mem_GPU" + str(i), 20)
        jetson.channels.append(i_data_channel)

        # add additional resources
        registers = SharedResource("registers", 65536 * 2)
        jetson.shared_resources.append(registers)

        # unrolling (comp. model)
        gpu.unrolling = create_jetson_gpu_comp_model(jetson, gpu)

        jetson.processors.append(gpu)

    ##################################
    #              CPU              #
    for i in range(0, 4):
        cpu = Processor("CPU" + str(i), "CPU")
        cpu.max_perf = 16.28
        cpu.max_power = 7.5 # Watt
        cpu.frequency = 2035

        cpu.parallel_comp_matrix = [8]

        # add local memories

        # add local input_examples transfer channels

        # mapping
        # memory mapping here
        cpu.data_on_memory_mapping = {
            "input_data": "l2_cache",
            "output_data": "l2_cache",
            "weights": "l2_cache",
        }

        # unrolling (comp. model)
        cpu.unrolling = create_jetson_unrolling_cpu(jetson, cpu)
        jetson.processors.append(cpu)

    return jetson


def create_jetson_unrolling_cpu(platform, processor):
    unrolling = DimsUnrolling()
    unrolling.loops_order = [3, 2, 0, 1, 4, 5]

    # unrolling.add_computations_unrolling(["ow"], [0])
    unrolling.add_computations_unrolling(["oh", "ow"], [0])

    inp_data_memory_name = processor.data_on_memory_mapping["input_data"]
    inp_data_memory = platform.get_memory(inp_data_memory_name)

    outp_data_memory_name = processor.data_on_memory_mapping["output_data"]
    outp_data_memory = platform.get_memory(outp_data_memory_name)

    w_data_memory_name = processor.data_on_memory_mapping["weights"]
    w_data_memory = platform.get_memory(w_data_memory_name)

    unrolling.add_limit(resource_user_name="output_data", resource=outp_data_memory,
                        dims=["ofm", "ow", "oh"], split_dims=["ofm"], load_dim="ofm", load=False)

    unrolling.add_limit(resource_user_name="weights", resource=w_data_memory,
                        dims=["ofm", "ifm", "kh", "kw"], split_dims=["ofm"], load_dim="ofm")

    unrolling.add_limit(resource_user_name="input_data", resource=inp_data_memory,
                        dims=["ifm", "ow", "oh"], split_dims=["ifm"], load_dim="ofm")

    return unrolling


def create_jetson_gpu_comp_model(platform, processor):
    unrolling = DimsUnrolling()
    unrolling.loops_order = [3, 2, 0, 1, 4, 5]
    unrolling.add_computations_unrolling(["oh", "ow"], [0])
    unrolling.add_computations_unrolling(["ifm"], [1])
    unrolling.add_computations_unrolling(["ofm"], [2])

    inp_data_memory_name = processor.data_on_memory_mapping["input_data"]
    inp_data = platform.get_memory(inp_data_memory_name)
    registers = platform.get_resource("registers")

    unrolling.add_limit(resource_user_name="input_data", resource=inp_data, dims=["ifm", "ow", "oh"], split_dims=["ifm"], load_dim="ofm")
    unrolling.add_limit(resource_user_name="registers", resource=registers, dims=["parallel0", "parallel1"], split_dims=["parallel1"], load_dim="ofm")

    return unrolling


def test():
    # build platform specifications, using generic EmbeddedPlatform class
    jetson = build_jetson_specification()
    jetson.print_details()

# test()