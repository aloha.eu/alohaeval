"""
This module is responsible for building CNN model for power/perf evaluation
"""
from dnn.dnn import DNN, Layer
from util import round_div


def build_dnn(eval_name: str) -> DNN:
    dnn = DNN(eval_name)
    blocks, structure = eval_name.split("-")
    n_stages = len(blocks) - 1

    depth = [int(blocks[i + 1]) for i in range(n_stages)]

    #What do we do if we have more stages?
    if n_stages == 5:
        max_depth = [2, 2, 4, 4, 4]
        stage_width = [64, 128, 256, 512, 512]
        stage_res = [32, 16, 8, 4, 2]
    elif n_stages == 3:
        max_depth = [3, 3, 4]
        stage_width = [64, 128, 256]
        stage_res = [32, 16, 8]


    #ifm for the first layer = 3 channels (RGB img)
    ifm = 3

    i = 0
    d = 0
    for stage_depth in depth:
        for j in range(stage_depth):
            #layer_OF.append(stage_width[i] * int(structure[d + j + 1]) / 8)  # building network structure
            #layer_res.append(stage_res[i])  # building network structure
            ofm = stage_width[i] * int(structure[d + j + 1]) / 8
            res = stage_res[i]
            layer = Layer(op="conv", ifm=ifm, ofm=ofm, res=res, fs=3)
            layer.ow = layer.oh = res
            layer.set_autopads()
            dnn.stack_layer(layer)
            #ifm for the next layer = ofm for current layer
            ifm = ofm
        d = d + max_depth[i]
        i = i + 1
    #  print(layer_OF)
    #  print(layer_res)
    #skip these layers for Jetson too?
    gemm0 = 4096 * (int(structure[-2]) / 8)
    gemm1 = 4096 * (int(structure[-1]) / 8)

    return dnn


def build_dnn_full(eval_name: str) -> DNN:
    dnn = DNN(eval_name)
    blocks, structure = eval_name.split("-")
    n_stages = len(blocks) - 1

    depth = [int(blocks[i + 1]) for i in range(n_stages)]

    #What do we do if we have more stages?
    if n_stages == 5:
        max_depth = [2, 2, 4, 4, 4]
        stage_width = [64, 128, 256, 512, 512]
        stage_res = [32, 16, 8, 4, 2]
    elif n_stages == 3:
        max_depth = [3, 3, 4]
        stage_width = [64, 128, 256]
        stage_res = [32, 16, 8]


    #ifm for the first layer = 3 channels (RGB img)
    ifm = 3

    i = 0
    d = 0
    for stage_depth in depth:
        for j in range(stage_depth):

            ofm = stage_width[i] * int(structure[d + j + 1]) / 8
            res = stage_res[i]

            if len(dnn.get_layers()) > 0:
                prev_layer = dnn.get_layers()[-1]
                # add max pooling to match resolution of current (new) layer and previous layer
                if prev_layer.res > res:
                    pooling_fs = round_div(prev_layer.res, res)
                    pooling_layer = Layer(op="pool", ifm=ifm, ofm=ifm, res=prev_layer.res, fs=pooling_fs)
                    pooling_layer.ow = pooling_layer.oh = res
                    dnn.stack_layer(pooling_layer)

            layer = Layer(op="conv", ifm=ifm, ofm=ofm, res=res, fs=3)
            layer.ow = layer.oh = res
            layer.set_autopads()
            dnn.stack_layer(layer)

            ifm = ofm
        d = d + max_depth[i]
        i = i + 1
    #  print(layer_OF)
    #  print(layer_res)
    #skip these layers for Jetson too?
    gemm0_ofm = 4096 * (int(structure[-2]) / 8)
    gemm1_ofm = 4096 * (int(structure[-1]) / 8)

    prev_layer = dnn.get_layers()[-1]
    prev_layer_total_outp = prev_layer.ofm * prev_layer.ow * prev_layer.oh

    # gemm 0
    layer = Layer(op="gemm", ifm=prev_layer.ofm, ofm=gemm0_ofm, res=prev_layer.res, fs=1)
    layer.ow = layer.oh = 1
    dnn.stack_layer(layer)

    # gemm 1
    prev_layer = dnn.get_layers()[-1]
    prev_layer_total_outp = prev_layer.ofm

    layer = Layer(op="gemm", ifm=prev_layer.ofm, ofm=gemm1_ofm, res=1, fs=1)
    layer.ow = layer.oh = 1
    dnn.stack_layer(layer)

    return dnn