##########################################################################
#                  SORTING and FILTERING                              ###

"""
Sort CNN stats
@point ids - x-axis
@values arrays - multiple y-axis, where each y-axis is time, memory or energy evaluation array
@sorting_plot_id specifies id of eval list, used for sorting. by default first list is used.
@ return list of sorted arrays [y1, y2...]
"""
def sort_eval_array(values_arrays, sorting_plot_id = 0):
    #print("sorting entered")
    #check if sorting is possible
    total_plots =  values_arrays.__len__()
    if(total_plots == 0):
        print("Array sorting impossible: total number of plots = 0")
        return

    plots_sorted = [ ] #n-dim array with sorted times


    for i in range(0, total_plots):
        sorted_plot = [ ]
        plots_sorted.append(sorted_plot)

    for pid in range (0, values_arrays[sorting_plot_id].__len__()):
        val = values_arrays[sorting_plot_id][pid]
        #get inster id for insert-based sorting
        ins_id = get_insertion_id (plots_sorted[sorting_plot_id], val)

        for plid in range(0, total_plots):
            val_i = values_arrays[plid][pid]
            val_plot = plots_sorted[plid]
            val_plot.insert(ins_id, val_i)
            #print("inserted")

    return plots_sorted


""" 
Find proper place for value point - used by insertion sorting above
@ values_sorted list of sorted values
@ value new value to be inserted into sorted values array
@ return id of value to be inserted
"""
def get_insertion_id(values_sorted, value):
    ins_id = 0
    for sorted_value in values_sorted:
        if value >= sorted_value:
            ins_id = ins_id + 1
        else: return ins_id
    return ins_id