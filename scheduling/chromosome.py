import random

#our mapping chromosome
class Chromosome:
    def __init__(self, processors_num, layers_num):
        self.mapping = []
        self.processors_num = processors_num
        self.layers_num = layers_num

        for i in range(self.processors_num):
            self.mapping.append([])

    def mutate(self):
        random_proc_id = random.randint(0, self.processors_num - 1)  # get random processor
        random_layer_id = random.randint(0, self.layers_num - 1)  # get random layer
        old_proc_id = self.get_proc(random_layer_id) #get current mapping of the layer
        self.mapping[old_proc_id].remove(random_layer_id)
        self.mapping[random_proc_id].append(random_layer_id)
        #print("Mutate: move layer ", random_layer_id, "from proc", old_proc_id, "to proc", random_proc_id)

    def clean(self):
        self.mapping = []
        for i in range(self.processors_num):
            self.mapping.append([])

    def init_random(self, preset_gpu_probability =-1, gpu_id =-1):
        if preset_gpu_probability < 0:
            for layer_id in range(self.layers_num):
                random_proc_id = random.randint(0, self.processors_num - 1)  # get random processor
                self.mapping[random_proc_id].append(layer_id)
        else:
            for layer_id in range(self.layers_num):
                # roll a dice: should we move a layer to gpu?
                random_chance = random.uniform(0, 1)  # Random float x, 0 <= x < 1
                if random_chance <= preset_gpu_probability:
                    # the layer goes to GPU
                    self.mapping[gpu_id].append(layer_id)
                else:
                    # select processor randomly
                    random_proc_id = random.randint(0, self.processors_num - 1)  # get random processor
                    self.mapping[random_proc_id].append(layer_id)


    def print(self, processors_labels, layer_labels):
        for p in range(self.processors_num):
            print(processors_labels[p], " {")
            for l in self.mapping[p]:
                print(layer_labels[l])
            print("}")

    def print_short(self):
        for p in range(self.processors_num):
            print("proc: ", p, "layers: ", self.mapping[p])

    def get_proc(self, layer_id):
        for p in range(self.processors_num):
            if self.mapping[p].__contains__(layer_id):
                return p
        return None

    # Execution time = max (proc1, proc2, ..., procP),  since we assume all the processors to work in parallel
    def eval_time(self, processor_types, distinct_processor_types, times_eval_table):
        times_per_proc = self.eval_times_per_proc(processor_types, distinct_processor_types, times_eval_table)
        max_time = max(times_per_proc)
        return max_time


    # evaluate execution time per processor
    def eval_times_per_proc(self, processor_types, distinct_processor_types, times_eval_table):
        proc_times = []
        # print(times_eval_table.__len__(), times_eval_table[0].__len__(), times_eval_table[1].__len__(), times_eval_table[2].__len__())
        for p in range(self.processors_num):
            proc_type = processor_types[p]
            distinct_proc_type_id = get_proc_type_id(proc_type, distinct_processor_types)
            proc_time = 0

            # time per processor = sum(times of tasks executed on this processor), since we assume tasks on one processor to work sequentially
            for l in self.mapping[p]:
                node_time = times_eval_table[distinct_proc_type_id][
                    l]  # find time for node l, executed on processor p of type proc_type_id
                proc_time = proc_time + node_time  # accumulate time for processor

            proc_times.append(proc_time)

            #print("Processor: ", p, " (type = ", distinct_processor_types[distinct_proc_type_id] , ") evaluated")
        return proc_times


#get processor type id
def get_proc_type_id(processor_type, distinct_processor_types):
    for i in range(distinct_processor_types.__len__()):
        if processor_type == distinct_processor_types[i]:
            return i
    return None