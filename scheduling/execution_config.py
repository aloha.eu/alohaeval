"""
This module contains a DNN execution config. The DNN execution config specifies
details of a DNN execution at the edge, using:
    - distribution of CNN operators over the target platform processors, represented as a
    dictionary with every key = CNN operator, every value = list of processor types, supporting this operator.
    In for an operator op no record is found in the distribution, we assume, that the operator can be executed on
    any available processor;
    - exploitation of task-level (pipeline) parallelism, available among layers of a CNN: boolean flag, determining
    type of scheduling. If pipeline = true, pipeline schedule is built, otherwise sequential schedule is built
"""


class ExecConfig:
    def __init__(self):
        self.pipeline = False
        self.ops_dist = {}

    def __str__(self):
        return "{\"pipeline\": " + str(self.pipeline) + ", \"ops_dist\": " + str(self.ops_dist) + "}"