import json

"""TODO:
Get communication penalty for processor
@proc tasks tasks mapped on processor
@tasks_adjacent_list adjacent_list of tasks output
   connections with len = number of nodes on graph, len[i] = number of
   output connections of graph node i

@communication_speed = tokens/ms
"""


class TaskGraph:
    def __init__(self, layers, tasks_adjacent_list, tasks_reverse_adjacent_list, tasks_out_comm_cost):
        """
        Create new task graph for a dnn
        :param layers list of layers names in traverse order i.e. layers =
        ["conv1_relu", "conv2_relu", "conv3_relu", "conv4_relu", "conv5_relu", "FC1_relu", "FC2_relu", "FC3", "Softmax"]
        :param tasks_adjacent_list - graph connectivity - outputs list
        E.g. list [[1, 2], [3], [4], [5], [5], [6], []] represents DNN graph

            1 - 3
          /       \
        0          5 - 6
          \       /
            2 - 4

        :param tasks_reverse_adjacent_list adjacent_list of layers input connections, e.g.
        for graph above tasks_reverse_adjacent_list = [[], [0], [0], [1], [2], [3, 4], [5]]
        :param tasks_out_comm_cost - number of tokens to write per node
        always has last element = 0 because output layer never writes
        """
        self.layers = layers
        self.layers_num = layers.__len__()
        self.tasks_adjacent_list = tasks_adjacent_list
        self.tasks_reverse_adjacent_list = tasks_reverse_adjacent_list
        self.tasks_out_comm_cost = tasks_out_comm_cost
        self.groups = None #layer groups are important for chained mapping


def parse_task_graph_json(path):
    """
    Parse application graph from JSON file
    :param path path to json file
    """
    with open(path, 'r') as file:
        if file is None:
            raise FileNotFoundError
        else:
            topology = json.load(file)
            layers = topology["task_names"]
            tasks_adjacent_list = topology["adjacency_lists"]
            tasks_reverse_adjacent_list = topology["adjacency_lists_reverse"]
            tasks_out_comm_cost = topology["output_connection_weighs"]

            app_graph = TaskGraph(layers, tasks_adjacent_list, tasks_reverse_adjacent_list, tasks_out_comm_cost)
            return app_graph


def get_example_graph():
    """
    Get simple CNN graph example

        1 - 3
      /       \
    0          5 - 6
      \       /
        2 - 4
    """
    layers = ["t0", "t1", "t2", "t3", "t4", "t5", "t6"]
    tasks_adjacent_list = [[1, 2], [3], [4], [5], [5], [6], []]
    tasks_reverse_adjacent_list = [[], [0], [0], [1], [2], [3, 4], [5]]
    connections_num = 7
    tasks_out_comm_cost = [0 for _ in range(0, connections_num)]

    app_graph = TaskGraph(layers, tasks_adjacent_list, tasks_reverse_adjacent_list, tasks_out_comm_cost)
    return app_graph

