from eval.dnn_layers_annotator import annotate_dnn_layers
import random


def build_eval_matrix(dnn, method, aloha_platform, lut_trees_per_processor={}):
    """
    Build time eval matrix from dnn description, while dnn is annotated with one of the perf. eval methods
    time_eval_matrix: is a matrix [M]x[N] where m in [0, len (processor_types_distinct)-1] represents processor type,
                    n in [0, layers_num] represents layer, matrix[m][n] contains execution time of layer n on processor m
    """
    if method not in ["ops", "lut", "sroof", "aloha"]:
        raise Exception("Unknown eval method " + method + ", choose from ['ops', 'lut', 'sroof', 'aloha']")

    eval_matrix = []

    distinct_proc_id = 0
    distinct_processor_names = find_distinct_processor_names(aloha_platform)
    for processor_name in distinct_processor_names:
        processor = aloha_platform.get_processor(processor_name)
        lut_tree = None
        if method == "lut":
            if processor.name in lut_trees_per_processor.keys():
                lut_tree = lut_trees_per_processor[processor.name]
                annotate_dnn_layers(dnn, lut_tree, aloha_platform, processor, method)
            else:
                # zero-time for unavailable lut
                for layer in dnn.get_layers():
                    layer.time_eval = 0

        else:
            annotate_dnn_layers(dnn, lut_tree, aloha_platform, processor, method)

        perf_estim_per_layer = [layer.time_eval for layer in dnn.get_layers()]
        eval_matrix.append(perf_estim_per_layer)
        distinct_proc_id = distinct_proc_id + 1

    return eval_matrix


def find_distinct_processor_names(aloha_platform):
    """
    Find all distinct representatives of heterogeneous cores in ALOHA platform
    :param aloha_platform: aloha platform
    :return: list of names of distinct representatives of heterogeneous cores in ALOHA platform
    """
    distinct_proc_types = []
    distinct_proc_names = []

    for processor in aloha_platform.processors:
        proc_name = processor.name
        proc_type = processor.type
        if proc_type not in distinct_proc_types:
            distinct_proc_names.append(proc_name)
            distinct_proc_types.append(proc_type)
    return distinct_proc_names


def get_sum_proc_time(eval_table, proc_tasks, proc_type_id):
    """Get total processor execution time"""
    sum_time = 0

    for proc_task in proc_tasks:
        sum_time += eval_table[proc_type_id][proc_task]

    return sum_time


def get_moc_eval_table(app_graph, platform, gpu_type_id):
    """
    Get moc evaluation table
    :param app_graph: application graph
    :param platform: platform
    :param gpu_id: id of GPU. If platform has no GPU, gpu_id = -1
    :return: mov evaluation table, generated for given application graph and platform randomly
    """
    eval_table = []
    proc_acceleration = 1
    proc_id = 0

    for proc_type in platform.processors_types_distinct:
        if proc_id == gpu_type_id:
            proc_acceleration = 10.0
        proc_eval = []

        for task in app_graph.layers:
            moc_task_time = random.uniform(0, 1) / proc_acceleration
            proc_eval.append(moc_task_time)

        proc_id = proc_id + 1
        eval_table.append(proc_eval)

    return eval_table
