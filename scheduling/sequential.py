
def build_sequential_mapping(dnn, platform, accelerator_name, ops_to_processors={}):
    """
    Builds sequential CNN execution config, where ops-to-processors specify
    distribution of CNN layers over processors. If ops to processors are unspecified,
    send everything on accelerator
    :param dnn: dnn
    :param platform: platform
    :param ops_to_processors: map, where key is CNN op and value is a distinct processor
    (i.e., if a platform has several GPUs: GPU0, GPU1,..., GPUn only GPU0 is specified in
    the ops_to_processors)
    :return: dnn sequential mapping
    """
    # create empty mapping
    mapping = [[] for i in range(len(platform.processors))]
    all_processor_names = [processor.name for processor in platform.processors]
    accelerator_proc_id = get_accelerator_proc_id(platform, accelerator_name)

    if len(ops_to_processors.values()) == 0:
        # dnn_seq_mapping = [[layer_id for layer_id in range(len(dnn.get_layers()))]]
        for layer_id in range(len(dnn.get_layers())):
            mapping[accelerator_proc_id].append(layer_id)
    else:
        layer_id = 0
        for layer in dnn.get_layers():
            if layer.op not in ops_to_processors.keys():
                proc_id = accelerator_proc_id
            else:
                proc_type_name = ops_to_processors[layer.op]
                # mapping, platform, proc_type_name, all_processors
                proc_id = find_next_proc_of_specified_type(mapping, platform, proc_type_name, all_processor_names)

            mapping[proc_id].append(layer_id)
            layer_id = layer_id + 1

    return mapping


def get_accelerator_proc_id(platform, accelerator_name):
    accelerator_id = -1
    proc_id = 0
    for processor in platform.processors:
        if processor.name == accelerator_name:
            accelerator_id = proc_id
        proc_id = proc_id + 1
    return accelerator_id


def find_next_proc_of_specified_type(mapping, platform, proc_type_name, all_processors):
    next_proc_id = -1
    proc_ids_of_specified_type = []
    for proc_id in range(len(all_processors)):
        processor = platform.processors[proc_id]
        if processor.type == proc_type_name:
            proc_ids_of_specified_type.append(proc_id)

    min_tasks_mapped = -1
    for proc_id_of_specified_type in proc_ids_of_specified_type:
        tasks_mapped = len(mapping[proc_id_of_specified_type])
        if min_tasks_mapped == -1 or min_tasks_mapped < tasks_mapped:
            next_proc_id = proc_id_of_specified_type
            min_tasks_mapped = tasks_mapped
    return next_proc_id





