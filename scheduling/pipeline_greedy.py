from scheduling.TaskGraph import get_example_graph
from scheduling.Architecture import get_jetson
from scheduling.eval_matrix_builder import get_moc_eval_table, get_sum_proc_time
from scheduling.partitioning_creator import get_all_partitions, print_partitions, layer_outputs, layer_inputs, external_outputs, has_external_outputs
"""
Greedy mapping algorithm accepts as input a list of CNN inference tasks and spreads the tasks over CPUs and GPUs,
using following steps:

1. All tasks are mapped on the most powerful processor: GPU (if available) or CPU
while execution time, associate with processors, is not balanced:
    2.1. Take next task from beginning of the task list. If beneficial
        (leads to overall CNN execution time reduction), map it on least busy processor
    2.2 Take next task from the beginning of the task list. If beneficial
        (leads to overall CNN execution time reduction), map it on least busy processor.
    If no tasks were moved from original processor, leave the loop and finish algorithm
"""


def map_greedy(app_graph, architecture, time_eval_matrix, accelerator_id, verbose=False):
    """ time_eval_matrix:
        # matrix [M]x[N] where m in [0, len (processor_types_distinct)-1] represents processor type,
        # n in [0, layers_num] represents layer, matrix[m][n] contains execution time of layer n on processor m
    """
    layers = app_graph.layers  # i.e. layers = ["conv1_relu", "conv2_relu", "conv3_relu", "conv4_relu", "conv5_relu", "FC1_relu", "FC2_relu", "FC3", "Softmax"]
    processors = architecture.processors  # i.e. ["CPU0", "CPU1", "CPU2", "CPU3", "CPU4", "GPU"]
    start_proc_id = 0
    if accelerator_id != -1:
        start_proc_id = accelerator_id

    mapping = [ [] for proc in range(len(processors))]

    #map everything on start_processor (GPU or first cpu)
    task_id = 0
    for task in layers:
        mapping[start_proc_id].append(task_id)
        task_id = task_id + 1

    proc_sum_time = get_sum_proc_times(mapping, architecture, time_eval_matrix)

    continue_migrate = True
    if verbose:
        print("CNN execution time on single processor: ", get_concurrect_execution_proc_times(proc_sum_time))
    while continue_migrate:
        most_busy_proc_id = get_most_busy_processor_id(mapping, architecture, time_eval_matrix)
        best_migration = get_most_beneficial_migration(mapping, architecture, app_graph, time_eval_matrix, most_busy_proc_id)

        if best_migration.beneficial():
            if verbose:
                print("task", best_migration.task_id, "will be migrated from processor", best_migration.src_proc_id,
                  "to processor", best_migration.dst_proc_id)
            migrate_task_continuous(mapping, architecture, app_graph, time_eval_matrix, best_migration)
        else:
            continue_migrate = False
            if verbose:
                print("no more beneficial migrations left")

    proc_sum_time = get_sum_proc_times(mapping, architecture, time_eval_matrix)
    if verbose:
        print("CNN execution time after migrations: ", get_concurrect_execution_proc_times(proc_sum_time))

    return mapping


def get_sum_proc_times(mapping, architecture, time_eval_matrix):
    sum_proc_times = [ ]
    proc_id = 0
    for proc_tasks in mapping:
        proc_type_id = architecture.get_proc_type_id(proc_id)
        sum_proc_time = get_sum_proc_time(time_eval_matrix, proc_tasks, proc_type_id)
        sum_proc_times.append(sum_proc_time)
        proc_id = proc_id + 1
    return sum_proc_times


def get_concurrect_execution_proc_times(sum_proc_times):
    return max(sum_proc_times)


def get_most_busy_processor_id(mapping, architecture, time_eval_matrix):
    """Get id of most busy processor"""
    sum_proc_times = get_sum_proc_times(mapping, architecture, time_eval_matrix)
    most_busy_proc_id = 0
    max_time = sum_proc_times[0]

    for proc_id in range(len(architecture.processors)):
        time = sum_proc_times[proc_id]
        if time > max_time:
            most_busy_proc_id = proc_id

    return most_busy_proc_id



def get_task_migration_benefit(task_id, mapping, architecture, app_graph, time_eval_matrix, src_proc_id, dst_proc_id):

    if migration_leads_to_cycles(task_id, mapping, src_proc_id, dst_proc_id):
        return -1

    """Predict execution time changes, that will occur if first task on the most busy processor is migrated to the least busy processor"""
    exec_times = get_sum_proc_times(mapping, architecture, time_eval_matrix)
    src_time = exec_times[src_proc_id]
    dst_time = exec_times[dst_proc_id]

    src_proc_type = architecture.get_proc_type_id(src_proc_id)
    dst_proc_type = architecture.get_proc_type_id(dst_proc_id)

    src_comm_penalty = 0 #get_communication_penalty(task_id, src_proc_id, mapping, architecture, app_graph)
    dst_comm_penalty = 0 #get_communication_penalty(task_id, dst_proc_id, mapping, architecture, app_graph)

    next_src_time = src_time - time_eval_matrix[src_proc_type][task_id] - src_comm_penalty
    next_dst_time = dst_time + time_eval_matrix[dst_proc_type][task_id] + dst_comm_penalty


    if next_src_time <= src_time and next_src_time > next_dst_time:
        return src_time - next_src_time
    else: return -1

    #return cur_cnn_exec_time - predicted_cnn_exec_time


def migration_leads_to_cycles(task_id, mapping, src_proc_id, dst_proc_id):
    """
    Check if migration can lead to cycles
    :param task_id:
    :param mapping:
    :param src_proc_id:
    :param dst_proc_id:
    :return:
    """
    dst_proc_tasks = mapping[dst_proc_id]
    #no cycles, if nothing mapped on the destination processor yet
    if len(dst_proc_tasks) == 0:
        return False

    #no cycles, if mapping continues chain
    if (task_id-1) in dst_proc_tasks or (task_id + 1) in dst_proc_tasks:
        return False

    return True



def get_communication_penalty(task_id, proc_id, mapping, architecture, app_graph):
    comm_speed = architecture.communication_speed
    """
    TODO: set GPU id as parameter
    """
    gpu_id = 5
    inputs = layer_inputs(task_id, app_graph.tasks_adjacent_list)
    outputs = layer_outputs(task_id, app_graph.tasks_adjacent_list)

    comm_cost = 0.0
    if proc_id == gpu_id:
        #count comm cost for communicating with every output, that is not mapped on GPU
        for outp in outputs:
            if outp not in mapping[gpu_id]:
                comm_cost += app_graph.tasks_out_comm_cost[task_id]

        # count comm cost for communicating with every input, that is not mapped on GPU
        for inp in inputs:
            if inp not in mapping[gpu_id]:
                comm_cost += app_graph.tasks_out_comm_cost[inp]

    comm_cost = comm_cost / comm_speed
    return comm_cost



class Migration:
    """
    Class describes time benefit of task migration from source processor to destination processor
    """
    def __init__(self, src_proc_id, dst_proc_id, is_head, task_id = -1, time_benefit = 0):
        self.src_proc_id = src_proc_id
        self.dst_proc_id = dst_proc_id
        self.is_head = is_head # if task is taken from the head
        self.task_id = task_id
        self.time_benefit = time_benefit

    def beneficial(self):
        return self.time_benefit >= 0 and self.task_id!=-1


def get_most_beneficial_migration(mapping, architecture, app_graph, time_eval_matrix, most_busy_proc_id):
    """
    Get id of the task to migrate
    :param mapping:
    :param architecture:
    :param time_eval_matrix:
    :param most_busy_proc_id:
    :param least_busy_proc_id:
    :return:
    """
    src_proc_id = most_busy_proc_id
    best_dst_proc_id = 0
    best_migration_task_id = 0

    head_task = mapping[src_proc_id][0]
    tail_task = mapping[src_proc_id][-1]

    time_benefit = -1
    is_head = True
    for dst_proc_id in range(len(architecture.processors)):
        if dst_proc_id!=src_proc_id:
            head_migration_time_benefit = get_task_migration_benefit(head_task, mapping, architecture, app_graph, time_eval_matrix, most_busy_proc_id, dst_proc_id)
            if head_migration_time_benefit >= 0 and head_migration_time_benefit > time_benefit:
                best_dst_proc_id = dst_proc_id
                best_migration_task_id = head_task
                time_benefit = head_migration_time_benefit
                is_head = True

            tail_migration_time_benefit = get_task_migration_benefit(tail_task, mapping, architecture, app_graph, time_eval_matrix, most_busy_proc_id, dst_proc_id)
            if tail_migration_time_benefit >= 0 and tail_migration_time_benefit > time_benefit:
                best_dst_proc_id = dst_proc_id
                best_migration_task_id = tail_task
                time_benefit = tail_migration_time_benefit
                is_head = False


    #migrate nothing if time benefit = 0 (there is no time benefit), or time benefit <0 (time would be only worser after migration)
    if time_benefit < 0:
        no_migration = Migration(src_proc_id, src_proc_id, is_head, -1, 0)
        return no_migration

    best_migration = Migration(src_proc_id, best_dst_proc_id, is_head, best_migration_task_id, time_benefit)
    return best_migration


def migrate_task(mapping, migration):
    """
    Migrate task from source to destonation processor
    """
    mapping[migration.src_proc_id].remove(migration.task_id)
    mapping[migration.dst_proc_id].append(migration.task_id)


def migrate_task_continuous(mapping, architecture, app_graph, time_eval_matrix, start_migration):
    """
    Migrate task from source to destination processor.
    If the task is a head-task, continue with migration all following tasks as long as migration is beneficial
    If the task is a tail-task, continue with migrating all previous tasks as long as migration is beneficial
    """
    migrate_task(mapping, start_migration)

    if start_migration.is_head:
        migrate_next_tasks(mapping, architecture, app_graph, time_eval_matrix, start_migration)
    else:
        migrate_prev_tasks(mapping, architecture, app_graph, time_eval_matrix, start_migration)


def get_next_task(mapping, proc_id, task_id):
    tasks = mapping[proc_id]
    for proc_task_id in tasks:
        if proc_task_id > task_id:
            return proc_task_id
    return None


def get_prev_task(mapping, proc_id, task_id):
    tasks_reverse = [task for task in mapping[proc_id]]
    tasks_reverse.reverse()
    for proc_task_id in tasks_reverse:
        if proc_task_id < task_id:
            return proc_task_id

    return None


def migrate_prev_tasks(mapping, architecture, app_graph, time_eval_matrix, start_migration, verbose=False):
    """
    Migrate all prev tasks between two processors
    :param mapping:
    :param architecture:
    :param time_eval_matrix:
    :param start_migration:
    :return:
    """
    prev_task = get_prev_task(mapping, start_migration.src_proc_id, start_migration.task_id)
    if verbose:
        print("prev for task", start_migration.task_id, "is task", prev_task)

    if prev_task is None:
        return

    benefit = get_task_migration_benefit(prev_task, mapping,architecture, app_graph, time_eval_matrix, start_migration.src_proc_id, start_migration.dst_proc_id)
    planned_migration = Migration(start_migration.src_proc_id, start_migration.dst_proc_id, False, prev_task, benefit)

    if planned_migration.beneficial():
        migrate_task(mapping, planned_migration)
        if verbose:
            print("prev task", prev_task, "is continuously migrated from proc", planned_migration.src_proc_id, "to proc", planned_migration.dst_proc_id)
        migrate_prev_tasks(mapping, architecture, app_graph, time_eval_matrix, planned_migration)


def migrate_next_tasks(mapping, architecture, app_graph, time_eval_matrix, start_migration, verbose=False):
    """
    Migrate all next tasks between two processors
    :param mapping:
    :param architecture:
    :param time_eval_matrix:
    :param start_migration:
    :return:
    """
    next_task = get_next_task(mapping, start_migration.src_proc_id, start_migration.task_id)
    if verbose:
        print("next for task", start_migration.task_id, "is task", next_task)

    if next_task is None:
        return

    benefit = get_task_migration_benefit(next_task, mapping, architecture, app_graph, time_eval_matrix, start_migration.src_proc_id,start_migration.dst_proc_id)
    planned_migration = Migration(start_migration.src_proc_id, start_migration.dst_proc_id, True, next_task, benefit)

    if planned_migration.beneficial():
        migrate_task(mapping, planned_migration)
        mapping[planned_migration.dst_proc_id].sort()
        if verbose:
            print("next task", next_task, "is continuously migrated from proc", planned_migration.src_proc_id, "to proc",
              planned_migration.dst_proc_id)
        migrate_next_tasks(mapping, architecture, app_graph, time_eval_matrix, planned_migration)


def find_proc(mapping, task_id):
    for proc_id in range(len(mapping)):
        if task_id in mapping[proc_id]:
            return proc_id


def test():
    app_graph = get_example_graph()
    architecture = get_jetson()
    gpu_id = 5
    gpu_type_id = architecture.get_proc_type_id(gpu_id)
    time_eval_matrix = get_moc_eval_table(app_graph, architecture, gpu_type_id)
    greedy_mapping = map_greedy(app_graph, architecture, time_eval_matrix, gpu_id)
    for map in greedy_mapping:
        map.sort()
        print(map)

    # partitions = get_all_partitions(greedy_mapping, app_graph.tasks_adjacent_list)
    # print_partitions(partitions, architecture.processors, app_graph.layers)


# test()

