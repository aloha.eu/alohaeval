
class Architecture:
    """
    Target architecture class: light-weighted target architecture description, used for scheduling
    @communication_speed = tokens/ms
    """
    def __init__(self, processors, processors_types,  processor_types_distinct, communication_speed):
        self.processors = processors #i.e. ["CPU0", "CPU1", "CPU2", "CPU3", "CPU4", "GPU"]
        self.processors_types = processors_types # required: len = len(processors), i.e. ["large_CPU", "large_CPU", "small_CPU", "small_CPU", "GPU"]
        self.processors_types_distinct = processor_types_distinct #i.e. ["large_CPU", "small_CPU", "GPU"]
        self.processor_types_distinct_num = processor_types_distinct.__len__()
        self.processors_num = processors.__len__()
        self.communication_speed = communication_speed

    # get processor type id
    def get_proc_type_id(self, processor_id):
        processor_type = self.processors_types[processor_id]
        for i in range(len(self.processors_types_distinct)):
            if processor_type == self.processors_types_distinct[i]:
                return i
        return None

    # get processor id
    def get_proc_id(self, processor_name):
        for i in range(len(self.processors)):
            if self.processors[i] == processor_name:
                return i
        return None


def get_jetson():
    """
    Get Jetson as architecture example
    """
    # info we shall get by parsing architecture file : list of processors
    processors = ["CPU0", "CPU1", "CPU2", "CPU3", "CPU4", "GPU"]
    processor_types = ["large_CPU", "large_CPU", "small_CPU", "small_CPU", "small_CPU", "GPU"]
    processor_types_distinct = ["large_CPU", "small_CPU", "GPU"]
    communication_speed = 2.5/1000000

    jetson = Architecture(processors, processor_types, processor_types_distinct, communication_speed)
    return jetson

