import argparse
import traceback
import os
import sys

"""
Console API file
"""

# test run examples:
# onnx file, time eval
# python eval_aloha.py --cnn input_examples/dnns/onnx_dnns/o34461.onnx -p input_examples/platforms/jetson.json -t

# ofa-encoded dnn, time, memory, energy eval
# python eval_aloha.py --cnn d22344-e686668808668868886 -p input_examples/platforms/jetson.json -t -m -e

# a directory with onnx files, time, memory, energy eval
# python eval_aloha.py --cnn input_examples/dnns/onnx_dnns -p input_examples/platforms/jetson.json -t -m

def main():
    # import current directory and it's subdirectories into system path for the current console
    # this would allow to import project modules without adding the project to the PYTHONPATH
    this_dir = os.path.dirname(__file__)
    sys.path.append(this_dir)

    # import project modules
    from parsers.json_util import read_json
    from parsers.platform_parser import parse_json_platform
    from eval.dnn_perf_estimator import estimate_dnns_metrics
    from eval.dnn_memory_evaluator import eval_dnn_list_memory

    # general arguments
    parser = argparse.ArgumentParser(description='The application performs evaluation of platform-aware metrics '
                                                 'of one or several Convolutional Neural Networks (CNNs), executed on '
                                                 'a heterogeneous multi-core target platform')

    parser.add_argument('--cnn', metavar='cnn', type=str, action='store', required=True,
                        help='path to one or several CNNs. Can be a path to: '
                             '1) an .onnx file; '
                             '2) a string with OFA CNN config; '
                             '3) a folder with several .onnx files; '
                             '4) a .json file with a list of OFA CNN configs')

    parser.add_argument('-p', metavar='platform', type=str, action='store',
                        help='path to a .json file with target platform description', required=True)

    parser.add_argument('-c', metavar='exec_config', type=str, action='store',
                        help='path to a .json file with CNN execution config', default=None)

    # general flags
    parser.add_argument("-l", help="evaluate latency (ms)", action="store_true", default=False)
    parser.add_argument("-t", help="evaluate throughput (fps)", action="store_true", default=False)
    parser.add_argument("-m", help="evaluate memory (MB)", action="store_true", default=False)
    parser.add_argument("-e", help="evaluate energy (J)", action="store_true", default=False)

    args = parser.parse_args()
    try:
        # extract parameters from command-line arguments
        cnn_path = args.cnn
        platform_path = args.p
        exec_config_path = args.c
        # extract flags from command-line arguments
        l, t, m, e = args.l, args.t, args.m, args.e

        if t is False and e is False and m is False:
            raise Exception("no metric specified. Select at least one metric from -t, -e, -m (time, energy, memory)")

        # parse platform
        platform_json = read_json(platform_path)
        platform = parse_json_platform(platform_json)

        # parse cnn(s)
        cnns = parse_cnns(cnn_path)

        # parse execution config
        exec_config = parse_execution_config(exec_config_path)

        # evaluation method always = aloha for this script
        e_time, e_throughput, e_energy, e_memory = estimate_dnns_metrics(cnns, method="aloha", platform=platform,
                                                                         pipeline=exec_config.pipeline,
                                                                         ops_to_processors=exec_config.ops_dist,
                                                                         estimate_time=l, estimate_energy=e,
                                                                         estimate_memory=m, estimate_throughput=t)

        for i in range(len(cnns)):
            eval_result = {"id:": i}
            if l:
                eval_result["latency (ms)"] = e_time[i]
            if t:
                eval_result["throughput(fps)"] = e_throughput[i]
            if e:
                eval_result["energy (J)"] = e_energy[i]
            if m:
                eval_result["memory (MB)"] = e_memory[i]

            print(eval_result)

    except Exception as e:
        print("ALOHA evaluation exception: " + str(e))
        traceback.print_tb(e.__traceback__)


def parse_cnns(cnn_path):
    """
    Parse input CNN(s)
    :param cnn_path: path to input CNN(s)
    :return: list of input CNN(s), where each CNN is an object of DNN class, defined in dnn.dnn.py
    """
    # try to parse CNN as onnx file
    if cnn_path.endswith(".onnx") or (os.path.isfile(cnn_path) and not cnn_path.endswith(".json")):
        from parsers.onnx_parser import read_onnx, onnx_to_dnn, set_dataflow
        onnx_cnn = read_onnx(cnn_path)
        onnx_cnn = set_dataflow(onnx_cnn)
        cnn = onnx_to_dnn(onnx_cnn)
        return [cnn]

    # try to parse CNN as OFA model config
    if cnn_path.startswith("d") and len(cnn_path) == len("d22344-e686668808668868886"):
        from builders.OFA_dnn_builder import build_dnn_full
        cnn = build_dnn_full(cnn_path)
        return [cnn]

    # try to parse CNNs as a set of onnx files
    if os.path.isdir(cnn_path):
        from parsers.onnx_parser import read_onnx, onnx_to_dnn, set_dataflow
        cnns = []
        all_files_in_dir = [f for f in os.listdir(cnn_path) if os.path.isfile(os.path.join(cnn_path, f))]
        onnx_file_names_in_dir = [f for f in all_files_in_dir if f.endswith(".onnx")]
        for onnx_file_name in onnx_file_names_in_dir:
            onnx_file_path = os.path.join(cnn_path, onnx_file_name)
            onnx_cnn = read_onnx(onnx_file_path)
            onnx_cnn = set_dataflow(onnx_cnn)
            cnn = onnx_to_dnn(onnx_cnn)
            cnns.append(cnn)

        return cnns

    # try to parse CNN as a .json file, which has list of ofa dnns?
    if cnn_path.endswith(".json") or (os.path.isfile(cnn_path) and not cnn_path.endswith(".onnx")):
        pass

    raise Exception("Cannot parse input CNN(s) " + cnn_path)


def parse_execution_config(exec_config_path):
    """
    Parse CNN execution configuration
    :param exec_config_path: path to .json execution configuration or None (empty config)
    :return: CNN execution configuration
    """
    from scheduling.execution_config import ExecConfig
    if exec_config_path is None:
        default_exec_config = ExecConfig()
        return default_exec_config
    from parsers.json_util import read_json
    from parsers.exec_config_parser import parse_json_exec_config
    exec_config_json = read_json(exec_config_path)
    exec_config = parse_json_exec_config(exec_config_json)
    return exec_config


if __name__ == "__main__":
    main()


