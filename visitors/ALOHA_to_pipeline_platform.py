"""
This module is building a simple platform description, used for scheduling scheduling
from ALOHA platform description, used for power-perf evaluation
"""
from platform_spec.platform_spec import EmbeddedPlatform
from scheduling.Architecture import Architecture


def build_pipeline_platform(aloha_p: EmbeddedPlatform, accelerator=None):
    processors = []
    processor_types = []
    processor_types_distinct = []
    for processor in aloha_p.processors:
        processors.append(processor.name)
        proc_type = processor.type
        processor_types.append(proc_type)
        if proc_type not in processor_types_distinct:
            processor_types_distinct.append(proc_type)

    # TODO: extract com. speed from accelerator-to-main memory desc.
    communication_speed = 2.5/1000000

    # if accelerator is not None:
    # communication_speed = 2.5/1000000

    pipeline_p = Architecture(processors, processor_types, processor_types_distinct, communication_speed)
    return pipeline_p

