from dnn.dnn import DNN, Layer
from scheduling.TaskGraph import TaskGraph
"""
This module is building a simpleDNN graph description, used for scheduling scheduling
from dnn description
"""

"""
Get simple CNN graph example

    1 - 3
  /       \
0          5 - 6
  \       /
    2 - 4

def getExampleGraph():
    layers = ["t0", "t1", "t2", "t3", "t4", "t5", "t6"]
    tasks_adjacent_list = [[1, 2], [3], [4], [5], [5], [6], []]
    tasks_reverse_adjacent_list = [[], [0], [0], [1], [2], [3, 4], [5]]
    connections_num = 7
    tasks_out_comm_cost = [0 for _ in range(0, connections_num)]

    app_graph = AppGraph(layers, tasks_adjacent_list, tasks_reverse_adjacent_list, tasks_out_comm_cost)
    return app_graph
"""


def dnn_to_task_graph(dnn):
    """
    Transform a dnn into a scheduling graph
    NOTE: DNN layers and edges should be sorted in traverse order!
    :return:
    """

    tasks = []
    tasks_adjacent_list = []
    tasks_reverse_adjacent_list = []

    dnn_layers = dnn.get_layers()
    dnn_connections = dnn.get_connections()

    for layer_id in range(len(dnn_layers)):
        layer = dnn_layers[layer_id]
        layer_name = "l" + str(layer_id)
        tasks.append(layer_name)

        l_inp_nodes_ids = []
        l_outp_nodes_ids = []

        layer_inp_connections = dnn.get_layer_input_connections(layer)
        for connection in layer_inp_connections:
            con_src = connection.src
            con_src_id = dnn.get_layer_id(con_src)
            l_inp_nodes_ids.append(con_src_id)

        layer_outp_connections = dnn.get_layer_output_connections(layer)
        for connection in layer_outp_connections:
            con_dst = connection.dst
            con_dst_id = dnn.get_layer_id(layer)
            l_outp_nodes_ids.append(con_dst_id)

        tasks_adjacent_list.append(l_outp_nodes_ids)
        tasks_reverse_adjacent_list.append(l_inp_nodes_ids)

    connections_num = len(dnn.get_connections())
    tasks_out_comm_cost = [0 for _ in range(0, connections_num)]

    app_graph = TaskGraph(tasks, tasks_adjacent_list, tasks_reverse_adjacent_list, tasks_out_comm_cost)
    return app_graph

