from util import mega
"""
This module evaluates dnn memory (in MB), taking into account DNN schedule and topology
"""


def eval_dnn_list_memory(dnns, bytes_per_pixel):
    """
    TODO: move to aggregation module. Take into account double buffering and pipeline execution.
    Eval memory of several dnns
    :param dnns: list of dnns
    :param bytes_per_pixel: bytes per wights/data pixel
    :return: DNN memory in megabytes (MB)
    """
    memory = []
    for dnn in dnns:
        dnn_memory = eval_dnn_memory(dnn, bytes_per_pixel)
        memory.append(dnn_memory)
    return memory


def eval_dnn_memory(dnn, bytes_per_pixel):
    """
    TODO: move to aggregation module. Take into account double buffering and pipeline execution.
    Eval dnn memory
    :param dnn: dnn
    :param bytes_per_pixel: bytes per wights/data pixel
    :return: DNN memory in megabytes (MB)
    """
    weights_tokens = dnn_weights_tokens(dnn)
    buffer_tokens = dnn_buffer_tokens(dnn)
    memory = (weights_tokens + buffer_tokens)/float(mega()) * bytes_per_pixel
    return memory


def dnn_buffer_tokens(dnn):
    """
    TODO: move to aggregation module. Take into account double buffering and pipeline execution.
    Get number of elements (tokens) in buffers of a DNN
    :param dnn: dnn
    :return: number of elements (tokens) in buffers of a DNN
    """
    tokens = 0
    for connection in dnn.get_connections():
        tokens += connection_buffer_tokens(connection)
    return tokens


def connection_buffer_tokens(connection):
    """
    Get number of elements (tokens) in a buffer of a DNN connection
    :param connection: dnn connection
    :return: number of elements (tokens) in a buffer of a DNN connection
    """
    src = connection.src
    buffer_tokens = src.ofm * src.oh * src.ow
    return buffer_tokens


def dnn_weights_tokens(dnn):
    """
    Get number of elements (tokens) in weights of a DNN
    :param dnn: dnn
    :return: number of elements (tokens) in weights of a DNN
    """
    weights = 0
    for layer in dnn.get_layers():
        weights += layer_weights_tokens(layer)
    return weights


def layer_weights_tokens(layer):
    """
    Get number of elements (tokens) in weights of a layer
    :param layer: layer
    :return: number of elements (tokens) in weights of a layer
    """
    weights = 0
    if layer.op == "conv":
        weights = layer.fs * layer.fs * layer.ifm * layer.ofm
        weights = weights + layer.ofm # bias

    if layer.op == "gemm":
        weights = layer.iw * layer.ih * layer.ifm * layer.ofm
        weights = weights + layer.ofm # bias

    if layer.op == "bn":
        weights = layer.ofm * 4 # scale + mean + variance + bias, each of layer.ofm tokens

    return weights

