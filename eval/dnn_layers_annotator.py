from eval.layer_perf_estimator import estim_layer_ops, estim_layer_lut, estim_layer_aloha, estim_layer_sroof


def annotate_dnn_layers(dnn, lut_tree=None, platform=None, processor=None, method="ops"):
    """
    Annotate dnn layers with performance estimation
    :param dnn: dnn
    :param lut_tree: lookup-tree
    :param platform: target platform
    :param processor: processor
    :param method: estimation method
    """
    if method == "ops":
        processor_max_performance = processor.max_perf
        for layer in dnn.get_layers():
            time = estim_layer_ops(layer, processor_max_performance)
            layer.time_eval = time
        return

    if method == "lut":
        for layer in dnn.get_layers():
            time_eval = estim_layer_lut(layer, lut_tree)
            layer.time_eval = time_eval
        return

    if method == "sroof":
        for layer in dnn.get_layers():
            time = estim_layer_sroof(layer, platform, processor, eval_data=True, verbose=False)
            layer.time_eval = time
        return

    if method == "aloha":
        for layer in dnn.get_layers():
            time = estim_layer_aloha(layer, platform, processor, eval_data=True, tile_data=True, verbose=False)
            layer.time_eval = time
        return

    raise Exception("unknown evaluation method: ", method)