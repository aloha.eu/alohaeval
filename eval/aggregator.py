from util import milli, mega
from eval.dnn_memory_evaluator import dnn_weights_tokens, connection_buffer_tokens
"""
This module aggregates performance evaluations of DNN layers
to compute total DNN performance
"""


def aggregate_dnn_performance(schedule, eval_matrix, processor_type_ids):
    """
    Aggregate performance estimation of a DNN
    :param schedule: CNN schedule (layers execution order)
    :param eval_matrix: evaluation matrix (see scheduling.eval_matrix_builder)
    :param processor_type_ids dictionary, where every key is
    processor_id (unique), every value is processor_type_id (repetitive)
    :return: DNN performance
    """
    times_per_processor = []
    for processor_id in range(len(schedule)):
        tasks_per_processor = schedule[processor_id]
        processor_type_id = processor_type_ids[processor_id]
        time_per_processor = 0
        for layer_id in tasks_per_processor:
            layer_perf = eval_matrix[processor_type_id][layer_id]
            time_per_processor = time_per_processor + layer_perf
        times_per_processor.append(time_per_processor)

    total_perf = max(times_per_processor)
    return total_perf


def aggregate_dnn_energy(schedule, aloha_platform, eval_matrix, processor_type_ids):
    """
    Aggregate energy estimation of a DNN
    :param schedule: CNN schedule (layers execution order)
    :param aloha_platform: aloha platform
    :param eval_matrix: evaluation matrix (see scheduling.eval_matrix_builder)
    :param processor_type_ids dictionary, where every key is
    processor_id (unique), every value is processor_type_id (repetitive)
    :return: DNN performance
    """
    joules_per_processor = []
    for processor_id in range(len(schedule)):
        tasks_per_processor = schedule[processor_id]
        processor_type_id = processor_type_ids[processor_id]
        time_per_processor = 0
        for layer_id in tasks_per_processor:
            layer_perf = eval_matrix[processor_type_id][layer_id]
            time_per_processor = time_per_processor + layer_perf
        processor = aloha_platform.processors[processor_id]
        processor_joules = time_per_processor * milli() * processor.max_power
        joules_per_processor.append(processor_joules)

    total_energy = sum(joules_per_processor)
    return total_energy


def aggregate_dnn_memory(dnn, schedule, aloha_platform, processor_type_ids):
    """
    Aggregate memory estimation of a DNN
    :param dnn: DNN
    :param schedule: CNN schedule (layers execution order)
    :param aloha_platform: aloha platform
    :param processor_type_ids dictionary, where every key is
    processor_id (unique), every value is processor_type_id (repetitive)
    :return: DNN performance
    """
    weight_tokens = dnn_weights_tokens(dnn)
    layers = dnn.get_layers()

    buf_tokens_per_connection = []
    for processor_id in range(len(schedule)):
        tasks_per_processor = schedule[processor_id]
        for task in tasks_per_processor:
            src_layer = layers[task]
            layer_output_connections = dnn.get_layer_output_connections(src_layer)
            layer_destinations = [connection.dst.id for connection in layer_output_connections]
            for out_connection_id in range(len(layer_output_connections)):
                out_connection = layer_output_connections[out_connection_id]
                con_buf_tokens = connection_buffer_tokens(out_connection)
                dst_layer_id = layer_destinations[out_connection_id]
                buffer_multiplier = 2 if dst_layer_id not in tasks_per_processor else 1
                buf_tokens = con_buf_tokens * buffer_multiplier
                buf_tokens_per_connection.append(buf_tokens)
                
    total_buffer_tokens = sum(buf_tokens_per_connection)
    total_memory_mb = (weight_tokens + total_buffer_tokens)/float(mega()) * aloha_platform.data_bytes

    return total_memory_mb
