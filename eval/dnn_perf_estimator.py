from builders.LUT_builder import build_lut_tree_from_Jetson_benchmark
from eval.aggregator import aggregate_dnn_performance, aggregate_dnn_energy, aggregate_dnn_memory
from visitors.ALOHA_to_pipeline_platform import build_pipeline_platform
from visitors.dnn_to_task_graph import dnn_to_task_graph
from scheduling.eval_matrix_builder import build_eval_matrix, find_distinct_processor_names
from scheduling.pipeline_greedy import map_greedy
from scheduling.sequential import build_sequential_mapping
from util import milli

"""
This module estimates performance (exec. time), energy and memory of a dnn or set of dnns.
The estimation of latency and energy is performed using one of the 
following estimation methods: 'ops', 'lut', 'sroof' or 'aloha'
"""


def estimate_dnns_metrics(dnns, method, platform, accelerator_name = None, lut_files_per_proc={},
                          pipeline=False, ops_to_processors={}, estimate_time=True, estimate_energy=True,
                          estimate_memory=True, estimate_throughput=True):
    """
    Estimate performance (milliseconds), energy (Joules) and memory (MegaBytes) of a set of dnns
    :param dnns: dnns
    :param method: power/performance evaluation method
    :param platform: (ALOHA) platform
    :param lut_files_per_proc: dictionary where key = distinct processor name, value = path to lookup-table file
    :param pipeline: scheduling mapping
    :param ops_to_processors mapping template where key = op name, value = processor type
    :param estimate_time: estimate DNN latency (in milliseconds)
    :param estimate_throughput: estimate DNN throughput (in frames per second)
    :param estimate_energy: estimate DNN energy cost (in Joules)
    :param estimate_memory: estimate DNN memory cost (in MegaBytes)
    :return: performance of dnns
    """
    if accelerator_name is None:
        accelerator_name = find_accelerator_name(platform)

    if method not in ["ops", "lut", "sroof", "aloha"]:
        raise Exception("Unknown eval method " + method + ", choose from ['ops', 'lut', 'sroof', 'aloha']")

    # dnn exec times to return
    dnn_times = []
    dnn_throughputs = []
    dnn_energies = []
    dnn_memories = []

    # build simplified platform model for scheduling
    scheduling_pla_model = build_pipeline_platform(platform)
    proc_type_ids = []
    for processor_id in range(len(platform.processors)):
        processor_type_id = scheduling_pla_model.get_proc_type_id(processor_id)
        proc_type_ids.append(processor_type_id)

    # specify accelerator id
    accelerator_id = scheduling_pla_model.get_proc_id(accelerator_name)

    # find all distinct processors
    distinct_proc_names = find_distinct_processor_names(platform)
    # print(distinct_proc_names)

    # build luts (for lut-based eval)
    lut_trees_per_processor = {}
    if method == "lut":
        for distinct_proc_name in distinct_proc_names:
            if distinct_proc_name in lut_files_per_proc.keys():
                lut_file = lut_files_per_proc[distinct_proc_name]
                lut_tree = build_lut_tree_from_Jetson_benchmark(lut_file)
                lut_trees_per_processor[distinct_proc_name] = lut_tree

    # estimate metrics for every dnn
    for dnn in dnns:
        # build evaluation matrix (time per every layer on every distinct processor)
        dnn_eval_matrix = build_eval_matrix(dnn, method, platform, lut_trees_per_processor)

        if pipeline:
            dnn_task_graph = dnn_to_task_graph(dnn)
            mapping = map_greedy(dnn_task_graph, scheduling_pla_model, dnn_eval_matrix, accelerator_id)
        else:
            mapping = build_sequential_mapping(dnn, platform, accelerator_name, ops_to_processors)

        # sort tasks in the mapping
        for tasks_per_processor in mapping:
            tasks_per_processor.sort()

        # aggregate and save dnn metrics
        dnn_evaluated_time = aggregate_dnn_performance(mapping, dnn_eval_matrix, proc_type_ids) if \
            estimate_time or estimate_throughput else 0
        dnn_time = dnn_evaluated_time if estimate_time else 0
        dnn_times.append(dnn_time)

        dnn_throughput = 1.0/(dnn_time * milli()) if estimate_throughput else 0
        dnn_throughputs.append(dnn_throughput)

        dnn_energy = aggregate_dnn_energy(mapping, platform, dnn_eval_matrix, proc_type_ids) if estimate_energy else 0
        dnn_energies.append(dnn_energy)

        dnn_memory = aggregate_dnn_memory(dnn, mapping, platform, proc_type_ids) if estimate_memory else 0
        dnn_memories.append(dnn_memory)

    return dnn_times, dnn_throughputs, dnn_energies, dnn_memories


def estimate_dnns_performance(dnns, method, platform, accelerator_name = None, lut_files_per_proc={}, pipeline=False,
                              ops_to_processors={}):
    """
    Estimate performance of dnns
    :param dnns: dnns
    :param method: evaluation method
    :param platform: (ALOHA) platform
    :param lut_files_per_proc: dictionary where key = disctinct processor name, value = path to lookup-table file
    :param pipeline: scheduling mapping
    :param ops_to_processors mapping template where key = op name, value = processor type
    :return: performance of dnns
    """
    if accelerator_name is None:
        accelerator_name = find_accelerator_name(platform)

    if method not in ["ops", "lut", "sroof", "aloha"]:
        raise Exception("Unknown eval method " + method + ", choose from ['ops', 'lut', 'sroof', 'aloha']")

    dnn_times, _, _ = estimate_dnns_metrics(dnns, method, platform, accelerator_name, lut_files_per_proc, pipeline,
                              ops_to_processors, estimate_time=True, estimate_energy=False, estimate_memory=False)

    return dnn_times


def find_accelerator_name(aloha_platform):
    # try find accelerator
    for processor in aloha_platform.processors:
        if processor.type in ["GPU", "Engine", "FPGA"]:
            accelerator_name = processor.name
            # print("accelerator: ", accelerator_name)
            return accelerator_name
    accelerator_name = aloha_platform.processors[0].name
    # print("accelerator: ", accelerator_name)
    return accelerator_name