"""
This module estimates precision of power/performance evaluation model
"""

import numpy as np

def estim_precision_percent(estimated_values: [float], real_values: [float]):
    errors = errors_percent(estimated_values, real_values)
    max_error = max(errors)
    avg_error = np.mean(errors)
    min_error = min(errors)
    print("error, %: min:", min_error, "max:", max_error, "avg", avg_error)




def avg_error_percent(estimated_values: [float], real_values: [float]) -> float:
    errors = errors_percent(estimated_values, real_values)
    error = np.mean(errors)
    return error


def errors_percent(estimated_values: [float], real_values: [float]) -> [float]:
    errors = []
    for value_id in range(len(estimated_values)):
        value_error = error_percent(estimated_values[value_id], real_values[value_id])
        errors.append(value_error)
    return errors


def error_percent(estimated_value: float, real_value: float) -> float:
    error = abs((real_value-estimated_value))/estimated_value * 100.0
    return error


def error_units(estimated_value: float, real_value: float):
    error = estimated_value - real_value
    return error
