from builders.platform_builder import build_jetson_specification, build_neuraghe_specification
from builders.ops_tensor_builder import build_ops_tensor
from eval.ops_tensor_transformer import apply_transformation_stages, print_as_nested_loops
from parsers.json_benchmark_parser import parse_jetson_layer_bm_record
from eval.layer_perf_estimator import estim_layer_aloha, analyze_aloha_ops_and_data_t

def conv_layer_example():
    """
    A bunch of simple conv layer examples for testing
    """
    json_layer = {"op": "conv", "kh": 1, "stride": 1, "ch": 128, "ofm": 512, "iw": 28, "oh": 28, "wpad": 0, "hpad": 0, "time": 0}
    layer = parse_jetson_layer_bm_record(json_layer)
    return layer


def test():
    jetson = build_jetson_specification()
    jetson_cpu = jetson.get_processor("CPU0")
    jetson_gpu = jetson.get_processor("GPU0")

    neuraghe = build_neuraghe_specification()
    neuraghe_engine = neuraghe.get_processor("Engine")

    layer = conv_layer_example()
    ops_t = build_ops_tensor(layer)
    print("platform-agnostic layer tensor: ")
    print_as_nested_loops(ops_t)

    print("************************************************************")
    print("PLATFORM-AWARE, JETSON CPU")
    apply_transformation_stages(layer, ops_t, jetson, jetson_cpu, verbose=True)

    print("************************************************************")
    print("PLATFORM-AWARE, JETSON GPU")
    apply_transformation_stages(layer, ops_t, jetson, jetson_gpu, verbose=True)


    print("************************************************************")
    print("PLATFORM-AWARE, NEURAGHE ENGINE")
    ops_t_refined, data_t_refined = apply_transformation_stages(layer, ops_t, neuraghe, neuraghe_engine, verbose=True)

    analyze_aloha_ops_and_data_t(ops_t_refined, data_t_refined, neuraghe, neuraghe_engine, True, True)


test()