from visitors.json_visitor import JSONNestedClassVisitor
from parsers.exec_config_parser import parse_json_exec_config
from os.path import dirname, join
from scheduling.execution_config import ExecConfig
from parsers.json_util import read_json


def create_config():
    e_conf = ExecConfig()
    e_conf.pipeline = False
    e_conf.ops_dist = {"conv": ["GPU"], "gemm": ["CPU"]}
    return e_conf


def dump_json_config():
    config = create_config()
    this_dir = dirname(__file__)
    path_rel = "../input_examples/exec_configs/config1.json"
    path_abs = join(this_dir, path_rel)
    visitor = JSONNestedClassVisitor(config, path_abs)
    visitor.run()


def read_json_config():
    this_dir = dirname(__file__)
    path_rel = "../input_examples/exec_configs/config1.json"
    path_abs = join(this_dir, path_rel)
    str_json = read_json(path_abs)
    exec_config = parse_json_exec_config(str_json)
    print(exec_config)


read_json_config()
# dump_json_config()

