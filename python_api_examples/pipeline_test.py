from builders.platform_builder import build_jetson_specification
from visitors.ALOHA_to_pipeline_platform import build_pipeline_platform
from visitors.dnn_to_task_graph import dnn_to_task_graph
from builders.OFA_dnn_builder import build_dnn
import os
from scheduling.eval_matrix_builder import build_eval_matrix, find_distinct_processor_names, get_sum_proc_time
from scheduling.pipeline_greedy import map_greedy
from builders.LUT_builder import build_lut_tree_from_Jetson_benchmark


def test():
    cur_dir = os.path.dirname(os.path.realpath('__file__'))
    lut_GPU = os.path.join(cur_dir, '../input_examples/luts/bm_GPU_trt.json')  # 'input_examples/bm_CPU_trt.json')
    lut_CPU = os.path.join(cur_dir, '../input_examples/luts/bm_ARM_CPU_PaolA_v2.json')  # 'input_examples/bm_CPU_trt.json')

    test_dnn_name = "d22344-e686668808668868886"
    test_dnn = build_dnn(test_dnn_name)

    platform = build_jetson_specification()
    accelerator = platform.get_processor("GPU0")

    pipeline_pla = build_pipeline_platform(platform)
    """
    print(pipeline_pla.processors)
    print(pipeline_pla.processors_types)
    print(pipeline_pla.processors_types_distinct)
    """

    dnn_task_graph = dnn_to_task_graph(test_dnn)
    """
    print(pipeline_app_graph.layers)
    print(pipeline_app_graph.tasks_adjacent_list)
    print(pipeline_app_graph.tasks_reverse_adjacent_list)
    """

    # dnn, method, aloha_platform, distinct_processor_names, luts_per_processor=None
    distinct_proc_names = find_distinct_processor_names(platform)
    # print(distinct_proc_names)

    lut_trees_per_processor = {}
    lut_tree_gpu = build_lut_tree_from_Jetson_benchmark(lut_GPU)
    lut_trees_per_processor["GPU0"] = lut_tree_gpu
    lut_tree_cpu = build_lut_tree_from_Jetson_benchmark(lut_CPU)
    lut_trees_per_processor["CPU0"] = lut_tree_cpu

    eval_matrix = build_eval_matrix(test_dnn, "lut", platform, lut_trees_per_processor)
    # print(eval_matrix)

    accelerator_id = 0
    accelerator_type_id = pipeline_pla.get_proc_type_id(0)
    accelerator_type = pipeline_pla.processors_types[accelerator_type_id]
    # print("accelerator_type:", accelerator_type)

    greedy_mapping = map_greedy(dnn_task_graph, pipeline_pla, eval_matrix, accelerator_id)

    for map in greedy_mapping:
        map.sort()

    """
    for map in greedy_mapping:
        print(map)
    """

    sum_proc_times = []
    for processor_id in range(len(greedy_mapping)):
        proc_type_id = pipeline_pla.get_proc_type_id(processor_id)
        sum_proc_time = get_sum_proc_time(eval_matrix, greedy_mapping[processor_id], proc_type_id)
        sum_proc_times.append(sum_proc_time)

    # print("sum proc times: ", sum_proc_times)
    max_proc_time = max(sum_proc_times)

    # print("max time: ", max_proc_time)

test()
