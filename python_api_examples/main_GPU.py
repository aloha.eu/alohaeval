from builders.OFA_dnn_builder import build_dnn
from input_examples.dnns.subnets1_asarray import subnet_names1
import os
from eval.dnn_perf_estimator import estimate_dnns_performance
from visualization.plt_basic import show_scatter_plot_multi_y
from builders.platform_builder import build_jetson_specification
from data_processing.sorting import sort_eval_array
from eval.estim_precision import estim_precision_percent, error_percent

subnet_names = subnet_names1
cur_dir = os.path.dirname(os.path.realpath('__file__'))
lut_file = os.path.join(cur_dir, '../input_examples/luts/bm_GPU_trt.json')#'input_examples/bm_CPU_trt.json')

dnns_list = []
for name in subnet_names:
    dnn = build_dnn(name)
    dnns_list.append(dnn)

# methods: ["ops", "lut", "sroof", "aloha"]:

platform = build_jetson_specification()
processor_name = "GPU0"

estim_lut = estimate_dnns_performance(dnns_list, "lut", platform, processor_name, {processor_name: lut_file}, pipeline=False)
estim_mops = estimate_dnns_performance(dnns_list, "ops", platform, processor_name, {processor_name: lut_file}, pipeline=False)
estim_sroof = estimate_dnns_performance(dnns_list, "sroof", platform, processor_name, {processor_name: lut_file}, pipeline=False)
estim_aloha = estimate_dnns_performance(dnns_list, "aloha", platform, processor_name, {processor_name: lut_file}, pipeline=False)
ids = [i for i in range(len(estim_lut))]

evals_sorted = sort_eval_array([estim_lut, estim_mops, estim_sroof, estim_aloha], 0) # estim_sroof, estim_aloha], 0)
show_scatter_plot_multi_y(ids, evals_sorted, "eval point (dnn) id", "latency")


error_mops = [error_percent(estim_mops[i], estim_lut[i]) for i in range(len(estim_aloha))]
error_sroof = [error_percent(estim_sroof[i], estim_lut[i]) for i in range(len(estim_aloha))]
error_aloha = [error_percent(estim_aloha[i], estim_lut[i]) for i in range(len(estim_aloha))]

estim_precision_percent(estim_aloha, estim_lut)
# show_means_and_errors_multi_bar([error_aloha], ["ALOHA"])