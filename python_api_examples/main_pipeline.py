from builders.OFA_dnn_builder import build_dnn
from input_examples.dnns.subnets1_asarray import subnet_names1
import os
from eval.dnn_perf_estimator import estimate_dnns_performance
from  visualization.plt_errors_and_means import show_means_and_errors_multi_bar
from builders.platform_builder import build_jetson_specification
from data_processing.sorting import sort_eval_array
from eval.estim_precision import estim_precision_percent, error_percent

subnet_names = subnet_names1
cur_dir = os.path.dirname(os.path.realpath('__file__'))
lut_gpu = os.path.join(cur_dir, '../input_examples/luts/bm_GPU_trt.json')
lut_cpu = os.path.join(cur_dir, '../input_examples/luts/bm_ARM_CPU_PaolA_v2.json')

dnns_list = []
for name in subnet_names:
    dnn = build_dnn(name)
    dnns_list.append(dnn)

# methods: ["ops", "lut", "sroof", "aloha"]:

platform = build_jetson_specification()
processor_name = "GPU0"

estim_lut_pipeline = estimate_dnns_performance(dnns_list, "lut", platform, processor_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=True)
estim_lut_sequential = estimate_dnns_performance(dnns_list, "lut", platform, processor_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=False)
estim_sroof = estimate_dnns_performance(dnns_list, "sroof", platform, processor_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=True)
estim_aloha_pipeline = estimate_dnns_performance(dnns_list, "aloha", platform, processor_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=True)
estim_aloha_seq = estimate_dnns_performance(dnns_list, "aloha", platform, processor_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=False)
ids = [i for i in range(len(estim_lut_pipeline))]

evals_sorted = sort_eval_array([estim_lut_pipeline, estim_lut_sequential, estim_sroof, estim_aloha_pipeline], 0) # estim_sroof, estim_aloha], 0)
# show_scatter_plot_multi_y(ids, evals_sorted, "eval point (dnn) id", "latency")

print("scheduling CNN execution latency prediction error, CPU-GPU")
print("LUT (sequential): ")
estim_precision_percent(estim_lut_sequential, estim_lut_pipeline)

print("ALOHA (scheduling): ")
estim_precision_percent(estim_aloha_pipeline, estim_lut_pipeline)

print("SROOF (scheduling): ")
estim_precision_percent(estim_sroof, estim_lut_pipeline)

error_lut_seq = [error_percent(estim_lut_sequential[i], estim_lut_pipeline[i]) for i in range(len(estim_aloha_pipeline))]
error_aloha_pipeline = [error_percent(estim_aloha_pipeline[i], estim_lut_pipeline[i]) for i in range(len(estim_aloha_pipeline))]
error_aloha_seq = [error_percent(estim_aloha_seq[i], estim_lut_pipeline[i]) for i in range(len(estim_aloha_pipeline))]

show_means_and_errors_multi_bar([error_lut_seq, error_aloha_seq, error_aloha_pipeline, ], ["LUT sequential", "ALOHA sequential", "ALOHA scheduling"])