from builders.OFA_dnn_builder import build_dnn_full
from input_examples.dnns.subnets1_asarray import subnet_names1
import os
from parsers.dnn_distinct_layers_parser import get_distinct_layers
from builders.platform_builder import build_jetson_specification
from builders.ops_tensor_builder import build_ops_tensor
from parsers.json_benchmark_parser import parse_jetson_layer_bm_record, parse_list
from dnn.dnn import DNN
from eval.dnn_perf_estimator import estimate_dnns_performance
from data_processing.sorting import sort_eval_array
from visualization.plt_basic import show_scatter_plot_multi_y
from visualization.plt_errors_and_means import show_means_and_errors_multi_bar
from eval.estim_precision import estim_precision_percent, error_percent

subnet_names = subnet_names1
cur_dir = os.path.dirname(os.path.realpath('__file__'))
lut_file_gemm_cpu = os.path.join(cur_dir, '../input_examples/luts/bm_CPU_trt_GEMM.json')#'input_examples/bm_CPU_trt.json')
lut_file_pool_cpu = os.path.join(cur_dir, '../input_examples/luts/bm_CPU_trt_POOL.json')#'input_examples/bm_CPU_trt.json')
lut_file = lut_file_pool_cpu

dnns_list = []
for name in subnet_names:
    dnn = build_dnn_full(name)
    dnns_list.append(dnn)
    # dnn.print_details()

# conv layer example
conv_layer_json = {"op": "conv", "kh": 3, "stride": 1, "ch": 48.0, "ofm": 96.0, "iw": 16, "oh":16,  "wpad": 1.0, "hpad": 1.0, "time": 0.100612}
conv_layer = parse_jetson_layer_bm_record(conv_layer_json)

# pooling examples
distinct_layers_pool = get_distinct_layers(dnns_list, allowed_ops=["pool"])
pool_layer = distinct_layers_pool[0]

# gemm examples
distinct_layers_gemm = get_distinct_layers(dnns_list, allowed_ops=["gemm"])
gemm_layer = distinct_layers_gemm[0]


layer = pool_layer
print("layer:", layer)

ops_t = build_ops_tensor(layer)
# print("platform-agnostic layer tensor: ", ops_t)
# print_as_nested_loops(ops_t)

jetson = build_jetson_specification()

jetson_cpu = jetson.get_processor("CPU0")
jetson_gpu = jetson.get_processor("GPU0")

platform = jetson
processor_name = "CPU0"

# ops_t_refined, data_t_refined = apply_transformation_stages(layer, ops_t, jetson, jetson_cpu, verbose=True)

# predict execution time for gemm/pool

# represent layers as one-dim dnns

bm_file_as_json_bm_list = parse_list(lut_file)
dnns_list = []
dnn_id = 0
# represent per-layer evaluation as evaluation of
# one-layer DNNs
for layer_json in bm_file_as_json_bm_list:
    layer = parse_jetson_layer_bm_record(layer_json)
    one_layer_dnn = DNN("DNN" + str(dnn_id))
    one_layer_dnn.stack_layer(layer)
    dnns_list.append(one_layer_dnn)
    dnn_id = dnn_id + 1

estim_lut = estimate_dnns_performance(dnns_list, "lut", None, processor_name, {processor_name: lut_file}, pipeline=False)
estim_mops = estimate_dnns_performance(dnns_list, "ops", platform, processor_name, {}, pipeline=False)
estim_sroof = estimate_dnns_performance(dnns_list, "sroof", platform, processor_name, {}, pipeline=False)
estim_aloha = estimate_dnns_performance(dnns_list, "aloha", platform, processor_name, {}, pipeline=False)
ids = [i for i in range(len(estim_lut))]

evals_sorted = sort_eval_array([estim_lut, estim_mops, estim_sroof, estim_aloha], 0) # estim_sroof, estim_aloha], 0)

show_scatter_plot_multi_y(ids, evals_sorted, "dnn", "latency")

error_mops = [error_percent(estim_mops[i], estim_lut[i]) for i in range(len(estim_aloha))]
error_sroof = [error_percent(estim_sroof[i], estim_lut[i]) for i in range(len(estim_aloha))]
error_aloha = [error_percent(estim_aloha[i], estim_lut[i]) for i in range(len(estim_aloha))]

estim_precision_percent(estim_aloha, estim_lut)
show_means_and_errors_multi_bar([error_aloha], ["ALOHA"])






