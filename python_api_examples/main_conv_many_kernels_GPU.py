import os
from eval.dnn_perf_estimator import estimate_dnns_performance
from visualization.plt_errors_and_means import show_means_and_errors_multi_bar
from builders.platform_builder import build_jetson_specification
from data_processing.sorting import sort_eval_array
from eval.estim_precision import estim_precision_percent, error_percent
from parsers.json_benchmark_parser import parse_list, parse_jetson_layer_bm_record
from dnn.dnn import DNN

cur_dir = os.path.dirname(os.path.realpath('__file__'))
lut_file = os.path.join(cur_dir, '../input_examples/luts/bm_GPU_trt_allconvs.json')

bm_file_as_json_bm_list = parse_list(lut_file)
dnns_list = []
dnn_id = 0
# represent per-layer evaluation as evaluation of
# one-layer DNNs
for layer_json in bm_file_as_json_bm_list:
    layer = parse_jetson_layer_bm_record(layer_json)
    one_layer_dnn = DNN("DNN" + str(dnn_id))
    one_layer_dnn.stack_layer(layer)
    dnns_list.append(one_layer_dnn)
    dnn_id = dnn_id + 1


platform = build_jetson_specification()
processor_name = "GPU0"

estim_lut = estimate_dnns_performance(dnns_list, "lut", None, processor_name, {processor_name: lut_file}, pipeline=False)
estim_mops = estimate_dnns_performance(dnns_list, "ops", platform, processor_name, {}, pipeline=False)
estim_sroof = estimate_dnns_performance(dnns_list, "sroof", platform, processor_name, {}, pipeline=False)
estim_aloha = estimate_dnns_performance(dnns_list, "aloha", platform, processor_name, {}, pipeline=False)
ids = [i for i in range(len(estim_lut))]

evals_sorted = sort_eval_array([estim_lut, estim_mops, estim_sroof, estim_aloha], 0) # estim_sroof, estim_aloha], 0)

# show_scatter_plot_multi_y(ids, evals_sorted, "dnn", "latency")

error_mops = [error_percent(estim_mops[i], estim_lut[i]) for i in range(len(estim_aloha))]
error_sroof = [error_percent(estim_sroof[i], estim_lut[i]) for i in range(len(estim_aloha))]
error_aloha = [error_percent(estim_aloha[i], estim_lut[i]) for i in range(len(estim_aloha))]

estim_precision_percent(estim_aloha, estim_lut)
show_means_and_errors_multi_bar([error_aloha], ["ALOHA"])
