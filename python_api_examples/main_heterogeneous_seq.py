from builders.OFA_dnn_builder import build_dnn_full
from input_examples.dnns.subnets1_asarray import subnet_names1
import os
from eval.dnn_perf_estimator import estimate_dnns_performance
from visualization.plt_errors_and_means import show_means_and_errors_multi_bar
from builders.platform_builder import build_jetson_specification
from data_processing.sorting import sort_eval_array
from eval.estim_precision import error_percent

subnet_names = subnet_names1
cur_dir = os.path.dirname(os.path.realpath('__file__'))
lut_gpu = os.path.join(cur_dir, '../input_examples/luts/bm_NAS_CONV_GEMM_POOL_GPU.json')#'input_examples/bm_CPU_trt.json')
lut_cpu = os.path.join(cur_dir, '../input_examples/luts/bm_NAS_CONV_GEMM_POOL_cpu.json')#'input_examples/bm_CPU_trt.json')

dnns_list = []
for name in subnet_names:
    dnn = build_dnn_full(name)
    dnns_list.append(dnn)

# methods: ["ops", "lut", "sroof", "aloha"]:

dnns_list_reduced = [dnns_list[0]]
# for dnn in dnns_list_reduced:
#    dnn.print_details()

platform = build_jetson_specification()
accelerator_name = "GPU0"

estim_lut_heterogeneous = estimate_dnns_performance(dnns_list, "lut", platform, accelerator_name, {"GPU0": lut_gpu, "CPU0": lut_cpu},
                                                    pipeline=False, ops_to_processors={"conv": "GPU0", "gemm": "CPU0"})
estim_lut_homogeneous = estimate_dnns_performance(dnns_list, "lut", platform, accelerator_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=False)
estim_aloha_homogeneous = estimate_dnns_performance(dnns_list, "aloha", platform, accelerator_name, {"GPU0": lut_gpu, "CPU0": lut_cpu}, pipeline=False)
estim_aloha_heterogeneous = estimate_dnns_performance(dnns_list, "aloha", platform, accelerator_name, {"GPU0": lut_gpu, "CPU0": lut_cpu},
                                                    pipeline=False, ops_to_processors={"conv": "GPU0", "gemm": "CPU0"})

ids = [i for i in range(len(estim_lut_heterogeneous))]

evals_sorted = sort_eval_array([estim_lut_heterogeneous, estim_lut_homogeneous, estim_aloha_heterogeneous], 0) # estim_sroof, estim_aloha], 0)
# show_scatter_plot_multi_y(ids, evals_sorted, "eval point (dnn) id", "latency")

error_lut_homogeneous = [error_percent(estim_lut_homogeneous[i], estim_lut_heterogeneous[i]) for i in range(len(estim_lut_homogeneous))]
error_aloha_homogeneous = [error_percent(estim_aloha_homogeneous[i], estim_lut_heterogeneous[i]) for i in range(len(estim_lut_homogeneous))]
error_aloha_heterogeneous = [error_percent(estim_aloha_heterogeneous[i], estim_lut_heterogeneous[i]) for i in range(len(estim_lut_homogeneous))]

show_means_and_errors_multi_bar([error_lut_homogeneous, error_aloha_homogeneous, error_aloha_heterogeneous], ["LUT, no aggregation", "ALOHA, no aggregation", "ALOHA, aggregation"])
