from builders.platform_builder import build_jetson_specification, build_neuraghe_specification
from visitors.json_visitor import JSONNestedClassVisitor
from parsers.platform_parser import parse_json_platform
from os.path import dirname, join
import json


def read_json_platforms():
    this_dir = dirname(__file__)
    path_rel_jetson = "../input_examples/platforms/jetson.json"
    path_rel_neuraghe = "../input_examples/platforms/neuraghe.json"
    platform_paths = [join(this_dir, path_relative) for path_relative in [path_rel_jetson, path_rel_neuraghe]]
    platforms = []
    for platform_path in platform_paths:
        with open(platform_path) as json_file:
            str_json = json.load(json_file)
            # print(str_json)
            platform = parse_json_platform(str_json)
            platforms.append(platform)
            platform.print_details()


def generate_json_platforms():
    jetson = build_jetson_specification()
    neuraghe = build_neuraghe_specification()
    this_dir = dirname(__file__)
    output_file_rel_jetson = "../input_examples/platforms/jetson.json"
    output_file_rel_neuraghe = "../input_examples/platforms/neuraghe.json"
    output_file_abs_jetson = join(this_dir, output_file_rel_jetson)
    output_file_abs_neuraghe = join(this_dir, output_file_rel_neuraghe)
    visitor = JSONNestedClassVisitor(jetson, output_file_abs_jetson)
    visitor.run()
    visitor = JSONNestedClassVisitor(neuraghe, output_file_abs_neuraghe)
    visitor.run()


def print_platforms_to_console():
    jetson = build_jetson_specification()
    neuraghe = build_neuraghe_specification()

    print("JETSON PLATFORM: ")
    jetson.print_details()
    print()
    print("*************************************")
    print("NEURAGHE PLATFORM: ")
    neuraghe.print_details()

# generate_json_platforms()
# print_platforms_to_console()
# read_json_platforms()